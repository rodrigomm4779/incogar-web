<?php
/**
 * Index Page
 *
 * @package chade
 * @since 1.0.0
 *
 */
$post_heading 		= chade_get_options( 'post_heading' );
$post_heading_title = chade_get_options( 'post_heading_title', 'Blog' );
$post_sidebar 		= chade_get_options( 'post_sidebar', true );
$show_thumbnail 	= chade_get_options( 'post_thumbnail', false );

$content_width_class = $post_sidebar ? 'col-md-9' : 'col-md-12';

$sidebar_head = chade_get_options( 'header_sidebar', false );
$center_class = ( ! $sidebar_head ) ? 'centered' : '';

get_header();

while ( have_posts() ) : the_post();
	/* Post Author Data */
	$first_name = get_the_author_meta( 'first_name' );
	$last_name = get_the_author_meta( 'last_name' );
	$author_link = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );

	/* Post options */
	$meta_data = get_post_meta( get_the_ID(), 'post_options', true );

	/* Get Site options for breadcrumbs */
	$show_on_front = get_option( 'show_on_front' );
	$page_for_posts = get_option( 'page_for_posts' );
?>

<div class="layout--container">
	<?php if ( isset( $meta_data['post_heading'] ) && $meta_data['post_heading'] ): ?>
		<!-- Post heading -->
		<?php if ( ! empty( $meta_data['heading_text'] ) ): ?>
			<div class="page_header post-heading <?php echo esc_attr( $center_class ); ?>">
				<div class="container">
					<h1 class="page_header--title"><?php echo esc_html( $meta_data['heading_text'] ); ?></h1>
				</div>
			</div>
		<?php endif;

		chade_breadcrumbs();

    endif; ?>

	<section style="padding-top: 80px" class="section">
		<div class="container">
			<div class="row">
				<div class="<?php echo esc_attr( $content_width_class ); ?> col-xs-12 single-post">
					<!-- Post body -->
					<div class="article">
						<!-- Post header -->
						<header class="article--header">
							<h2 class="article--title">
                                <a href="<?php echo esc_url(get_the_permalink()); ?>"><?php the_title(); ?></a>
                            </h2>
							<ul class="article--meta">
								<li class="article--meta_item -date"><?php the_time( get_option('date_format') ); ?></li>
                                <?php if(!empty($first_name) || !empty($last_name)){ ?>
                                    <li class="article--meta_item -author"><?php esc_html_e( 'by ', 'chade' ); ?>
                                        <a href="<?php echo esc_url( $author_link ); ?>">
			                                <?php echo esc_html( $first_name ) . ' ' . esc_html( $last_name ); ?>
                                        </a>
                                    </li>
                                <?php }

                                if(get_comments_number() > 0){ ?>
								    <li class="article--meta_item -comments"><?php comments_number(); ?></li>
								<?php } ?>
								<li class="article--meta_item -categories"><?php the_category( ', ' ); ?></li>
							</ul>
						</header>
						<!-- Enad post header -->

						<!-- Post content area -->
						<div class="article--content">
							<?php the_content(); ?>
							<?php wp_link_pages('before=<div class="post-nav"> <span>' . esc_html__( 'Page:', 'chade' ) . ' </span> &after=</div>'); ?>
						</div>
						<!-- End post content area -->

						<!-- Post footer: tags and share buttons -->
                        <?php if(!empty(get_the_tags()) || function_exists( 'cs_framework_init' )){ ?>
                            <div class="article--footer">
	                            <?php if(!empty(get_the_tags())){ ?>
                                    <div class="tags">
                                        <span class="tags-links">
                                            <?php the_tags( '', '', '' ); ?>
                                        </span>
                                    </div>
	                            <?php }

	                            if( function_exists( 'cs_framework_init' )){ ?>
                                    <div class="share js-share">
                                        <button type="button" class="share--opener js-share--opener"> <i class="icons8-share"></i><?php esc_html_e( 'Share', 'chade' ); ?></button>
                                        <div class="share--dropdown js-share--dropdown">
                                            <div class="social-sharing is-clean">
                                                <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php esc_url(the_permalink()); ?>" class="share-facebook">
                                                    <span aria-hidden="true" class="icon icon-facebook"></span>
                                                    <span class="share-title"><?php esc_html_e( 'Share', 'chade' ); ?></span>
                                                </a>
                                                <a target="_blank" href="http://twitter.com/share?url=<?php esc_url(the_permalink()); ?>&amp;text=<?php echo urlencode( get_the_title() ); ?>" class="share-twitter">
                                                    <span aria-hidden="true" class="icon icon-twitter"></span>
                                                    <span class="share-title"><?php esc_html_e( 'Tweet', 'chade' ); ?></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
		                        <?php } ?>

                            </div>
                        <?php } ?>
						<!-- End post footer -->
					</div>
					<!-- Comments -->
					<?php comments_template(); ?>
					<!-- End comments -->
				</div>

				<?php if ( $post_sidebar ): ?>
					<!-- Blog sidebar -->
					<div class="col-md-3 col-xs-12">
						<div class="blog_sidebar">
							<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar('sidebar') ); ?>
						</div>
					</div>
					<!-- End blog sidebar -->
				<?php endif; ?>
			</div>
		</div>
	</section>
</div>

<?php endwhile;

get_footer(); ?>
