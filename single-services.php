<?php
/**
 * Services Page
 *
 * @package chade
 * @since 1.0.0
 *
 */
$post_heading       = chade_get_options( 'post_heading' );
$post_heading_title = chade_get_options( 'post_heading_title', 'Blog' );
$services_sidebar   = chade_get_options( 'services_sidebar', true );

$content_width_class = $services_sidebar ? 'col-md-9' : 'col-md-12';

get_header();

while ( have_posts() ) : the_post();
	$content = get_the_content();

	/* Post options */
	$meta_data = get_post_meta( get_the_ID(), 'services_options', true ); ?>

    <div class="layout--container">
		<?php if ( isset( $meta_data['post_heading'] ) && $meta_data['post_heading'] ): ?>
            <!-- Post heading -->
			<?php if ( ! empty( $meta_data['heading_text'] ) ): ?>
                <div class="page_header post-heading">
                    <div class="container">
                        <h1 class="page_header--title">
							<?php echo esc_html( $meta_data['heading_text'] ); ?>

                        </h1>
                    </div>
                </div>
			<?php endif;

			// old breadcrumbs
			chade_breadcrumbs();

		endif; ?>

        <section style="padding-top: 80px" class="section">
            <div class="container">
                <div class="row">
					<?php if ( $services_sidebar ): ?>
                        <!-- Services sidebar -->
                        <div class="col-md-3 col-xs-12">
                            <div class="service_sidebar">
								<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'services-sidebar' ) ); ?>
                            </div>
                        </div>
                        <!-- End services sidebar -->
					<?php endif; ?>
                    <div class="<?php echo esc_attr( $content_width_class ); ?> col-xs-12">
                        <!-- Service body -->
                        <div class="service_page">
							<?php if ( ! strpos( $content, 'vc_' ) && ! strpos( $content, 'woocommerce_' ) ) { ?>
                                <div class="service_description">
                                    <h2 class="service_description--title"><?php esc_html( the_title() ); ?></h2>
                                    <div class="service_description--text">
										<?php the_content(); ?>
                                    </div>
                                </div>
							<?php } else {
								the_content();
							} ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php endwhile;

get_footer(); ?>