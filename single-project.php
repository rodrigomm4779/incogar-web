<?php
/**
 * Study Page
 *
 * @package chade
 * @since 1.0.0
 *
 */

get_header();

while ( have_posts() ) : the_post();
	$content = get_the_content(); ?>

	<div class="layout--container">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<!-- Service body -->
					<?php if ( ! strpos( $content, 'vc_' ) && ! strpos( $content, 'woocommerce_' ) ) { ?>
					<div class="service_page">
						<div class="service_description">
							<h2 class="service_description--title">
                                <?php esc_html(the_title()); ?>
                            </h2>
							<div class="service_description--text">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
					<?php } else { ?>
						<?php the_content(); ?>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

<?php endwhile;
get_footer(); ?>