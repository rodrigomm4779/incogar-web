;(function ($, window, document, undefined) {
	"use strict";

	$('.custom-menu-type select').on('change',function(){
		showField( $(this) );
	});

	function showField( $this ){
		var $select = $this,
			$detailed = $select.closest('.menu-item-settings').find('.custom-post-type-detailed'),
			$detailedText = $select.closest('.menu-item-settings').find('.custom-post-type-detailed-text');
			
		if( $select.val() != 'default' ) {
			$detailed.show();
			$detailedText.show();
		} else {
			$detailed.hide();
			$detailedText.hide();
		}
	}

	$(window).load(function(){
		if( $('.custom-menu-type select').length ) {
			$('.custom-menu-type select').each(function(){
				showField( $(this) );
			});
		}
	});

})(jQuery, window, document);