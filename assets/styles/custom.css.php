<?php

header("Content-type: text/css; charset: UTF-8"); ?>

<?php

if (cs_get_option( 'preloader_items_color') && cs_get_option( 'preloader_items_color') !== '#ff5e14') : ?>
    .service_item--icon,
    .mega_nav--item.-icon .mega_nav--item_icon,
    .link,
    .footer_main a:hover,
    .pricing_plan--img,
    .promo_slider_nav--item_icon,
    .service_item_hover .service_item--title,
    .service_item_hover .service_item--footer .link,
    .service_item_hover .service_item--icon,
    .tab_navigation--link_text .-active,.tab_navigation--link.-active i,
    .tab_navigation--link:hover i,
    .tab_navigation--link.-active span,
    .tab_navigation--link:hover span,
    .article--meta_item:after,
    .promo_detailed--list_item_icon i,
    .pricing_plan--icon,
    .widget--title:before,
    .text-shortcode a,
    .article--content a,
    .comment-author-link,
    .widget ul li a:hover,
    .why_mini--item_number,
    .strong_points_item--more,
    .strong_points_item--icon,
    .twitter_feed--icon i,
    .client_review--job,
    .share--opener,
    .button.-yellow.-bordered ,
    .breadcrumbs--link.-active, 
    .promo_banner--footer .button,
    .slide_block--body .button,
    .header_nav .menu>.menu-item>a:hover,
    .header_nav .menu>.menu-item.current-menu-item>a, .header_nav .menu>.menu-item:hover>a,
    .header_nav .menu > .menu-item > a .menu-item-notify,
    .service_item.-horizontal .service_item--icon,
    .service_item--footer .link,
    .service_item_hover_blue .service_item--footer .link, 
    .service_item_hover_blue .service_item--icon,
    .blue_style .tab_navigation--link.-active i, 
    .blue_style .tab_navigation--link:hover i, 
    .blue_style .tab_navigation--link.-active span, 
    .blue_style .tab_navigation--link:hover span,
    .our_company--quote_text:before,
    .our_company--text ul li:before, 
    .our_mission--item_body ul li:before,
    .team_member--email a,
    .team_member--links li a:hover,
    .icontext--list_item dl dt,
    .icontext--list_item_icon i,
    #grid-1119 .tg-nav-color,
    .tg-nav-color,
    .our_company--faq_title-letter, 
    .our_company--faq_text-letter,
    .header_nav.-wide .header_nav--inner>.menu>.menu-item.current-menu-item>a,
    .widget_solutions li a .fa, 
    .widget_solutions li a [class*=icons],
    .why_mini--item_icon,
    .faq--item.-opened .faq--item_title,
    .faq--item_title:before,
    .pagination ul a.-active, 
    .page-numbers.current,
    .our_company--href_block a:hover,
    .careers_article--icon,
    .mec-wrap .mec-totalcal-box .mec-totalcal-view span:hover,
    .mec-color, 
    .mec-color-before :before,
    .mec-color-hover:hover, 
    .mec-wrap .mec-color, 
    .mec-wrap .mec-color-before :before, 
    .mec-wrap .mec-color-hover:hover,
    .contact_item--icon,
    .header_nav.-on_dark .header_nav--inner>.menu>.menu-item.current-menu-item>a,
    .header_nav.-on_dark .header_nav--inner>.menu>.menu-item>a:hover,
    .header_nav.-on_dark .header_nav--inner>.menu>.menu-item:hover>a,
    .widget_search label:before,
    .post-content .post-title a,
    .mec-event-sharing-wrap li i,
    .mec-events-masonry-cats a.mec-masonry-cat-selected {
        color: <?php echo esc_html(cs_get_option( 'primary_site_color')) ?>;
    }

    .mec-event-sharing-wrap li i:hover {
        color: <?php echo esc_html(cs_get_option( 'primary_site_color')) . 'CC' ?>;
    }

    .footer_contact_info--item i,
    .contact_mini--icon,
    #grid-1119 .tg-nav-color,
    .tg-nav-color,
    .vacancies--item_content a, 
    .vacancies--item_content-light a {
        color: <?php echo esc_html(cs_get_option( 'primary_site_color')) ?>!important;
    }

    .pricing_plan--label.-popular,
    .service_item_hover:hover,
    .promo_slider_nav--item .line,
    .clients.-dark .slick-slider .slick-dots>li button:before,
    .button.-yellow.-bordered:hover,
    .slick-slider .slick-dots>li button:before,
    .-on_dark .wpcf7-submit,
    .statistics-wrapper,
    .article.-blog_page>.post-cats a,
    .button.-red,
    .button.-white_orange:hover,
    .service_item_hover_blue:hover,
    .button.-blue_light,
    .slick-slider .slick-dots>li button:before,
    .clients_reviews-sm .slick-slider .slick-dots button:before,
    .button.-yellow,
    .link-wrapper a,
    .footer_contacts,
    .map--opener {
        background-color: <?php echo esc_html(cs_get_option( 'primary_site_color')) ?>;
    }
    
    .pricing_plan--label {
        background-color: <?php echo esc_html(cs_get_option( 'primary_site_color')) ?>!important;
    }
    
    .tab_navigation--link .line,
    .vacancies--item:before,
    .tab_navigation-- link:before,
    .slide_block--icon,
    .select_language--list>li>a:hover,
    .tab_navigation--link .tab_navigation--line,
    .services_info--item_order,
    .pagination ul a:before, 
    .page-numbers.current:before,
    body .booked-list-view button.button,
    .mec-wrap .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,
    .select2-results__option.select2-results__option--highlighted,
    .widget .tagcloud a:hover {
        background: <?php echo esc_html(cs_get_option( 'primary_site_color')) ?>;
    }

    body .booked-list-view button.button:hover {
        background: <?php echo esc_html(cs_get_option( 'primary_site_color')) . 'CC' ?>!important;   
    }

    .button.-yellow:hover,
    .-on_dark .wpcf7-submit:hover,
    .button.-red:hover,
    .blue_layer .case_studies--item:hover:after,
    .button.-blue_light:hover {
        background-color: <?php echo esc_html(cs_get_option( 'primary_site_color')) . 'CC' ?>;
    }

    .mec-event-sharing-wrap:hover>li {
        background-color: <?php echo esc_html(cs_get_option( 'primary_site_color')) . '26' ?>;
    }      

    .button.-yellow.-bordered,
    .button.-yellow.-bordered:hover,
    .field.-on_dark input:focus, 
    .field.-on_dark textarea:focus,
    .field input:focus, 
    .field textarea:focus,
    .header_nav .menu > .menu-item > a .menu-item-notify,
    .our_company--faq_title-letter, 
    .our_company--faq_text-letter,
    body .booked-list-view button.button,
    .mec-wrap .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,
    .mec-month-divider span:before,
    .widget .tagcloud a:hover,
    .mec-events-masonry-cats a.mec-masonry-cat-selected {
        border-color: <?php echo esc_html(cs_get_option( 'primary_site_color')) ?>;
    }

    body .booked-list-view button.button:hover {
        border-color: <?php echo esc_html(cs_get_option( 'primary_site_color')) ?>!important;   
    }

    .share--opener{
        border: 1px solid <?php echo esc_html(cs_get_option( 'primary_site_color')) ?>;
    }

    .text-shortcode blockquote,
    .article--content blockquote{
        border-left: 3px solid <?php echo esc_html(cs_get_option( 'primary_site_color')) ?>;
    }

<?php endif;

$post_id = isset($_GET['post']) && is_numeric($_GET['post']) ? $_GET['post'] : '' ;

if(!empty($post_id)){
$meta_data = get_post_meta( $post_id, 'second_footer_color', true );

    if (isset($meta_data['select_custom_second_footer_color']) && !empty($meta_data['select_custom_second_footer_color']) && ($meta_data['select_custom_second_footer_color']) == 'yes') {
        if (isset($meta_data['custom_second_footer_color']) && !empty($meta_data['custom_second_footer_color'] ) ) { ?>

            .footer_contacts,
            .tab_navigation--line,
            .statistics-wrapper,
            .slide_block--icon,
            .select_language--list>li>a:hover,
            .tab_navigation--link .line,
            .tab_navigation--link.-active .tab_navigation--line,
            .tab_navigation--link .tab_navigation--line,
            .vacancies--item:before,
            .services_info--item_order,
            .pagination ul a:before, 
            .page-numbers.current:before,
            body .booked-list-view button.button,
            .mec-wrap .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,
            .select2-results__option.select2-results__option--highlighted,
            .widget .tagcloud a:hover {
                background: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ); ?>;
            }

            body .booked-list-view button.button:hover {
                background: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ) . 'CC'; ?>!important;
            }

            .tab_navigation--link_text .-active,
            .tab_navigation--link.-active i,
            .tab_navigation--link:hover i,
            .tab_navigation--link.-active span,
            .tab_navigation--link:hover span,
            .article--,
            .contact_mini--icon,
            .footer_contact_info--item i,
            .strong_points_item--more,
            .header_nav.-on_dark .header_nav--inner > .menu > .menu-item > a:hover, .header_nav .menu > .menu-item > a:hover,
            .header_nav.-on_dark .header_nav--inner>.menu>.menu-item:hover>a,
            .mega_nav--item.-icon .mega_nav--item_icon,
            .header_nav .menu > .menu-item > a .menu-item-notify,
            .strong_points_item--icon,
            .promo_banner--footer .button,
            .why_mini--item_number,
            .service_item_hover .service_item--footer .link,
            .link,
            .service_item_hover .service_item--title,
            .service_item--icon,
            .service_item_hover .service_item--icon,
            .twitter_feed--icon i,
            .client_review--job,
            .promo_slider_nav--item_icon,
            .slide_block--body .button,
            .header_nav .menu>.menu-item:hover>a,
            .footer_main a:hover,
            .promo_detailed--list_item_icon i,
            .pricing_plan--icon,
            .service_item.-horizontal .service_item--icon,
            .service_item--footer .link,
            .service_item_hover_blue .service_item--footer .link,
            .blue_style .tab_navigation--link.-active i, 
            .blue_style .tab_navigation--link:hover i, 
            .blue_style .tab_navigation--link.-active span, 
            .blue_style .tab_navigation--link:hover span,
            .breadcrumbs--link.-active,
            .our_company--quote_text:before,
            .our_company--text ul li:before, 
            .our_mission--item_body ul li:before,
            .team_member--email a,
            .team_member--links li a:hover,
            .icontext--list_item dl dt,
            .icontext--list_item_icon i,
            .our_company--faq_title-letter, 
            .our_company--faq_text-letter,
            .header_nav.-wide .header_nav--inner>.menu>.menu-item.current-menu-item>a,
            .widget_solutions li a .fa, 
            .widget_solutions li a [class*=icons],
            .why_mini--item_icon,
            .faq--item.-opened .faq--item_title,
            .faq--item_title:before,
            .pagination ul a.-active, 
            .page-numbers.current,
            .article--meta_item:after,
            .careers_article--icon,
            .widget ul li a:hover,
            .widget--title:before,
            .comment-author-link,
            .our_company--href_block a:hover,
            .mec-wrap .mec-totalcal-box .mec-totalcal-view span:hover,
            .mec-color, 
            .mec-color-before :before,
            .mec-color-hover:hover, 
            .mec-wrap .mec-color, 
            .mec-wrap .mec-color-before :before, 
            .mec-wrap .mec-color-hover:hover,
            .contact_item--icon,
            .header_nav.-on_dark .header_nav--inner>.menu>.menu-item.current-menu-item>a,
            .header_nav.-on_dark .header_nav--inner>.menu>.menu-item>a:hover,
            .header_nav.-on_dark .header_nav--inner>.menu>.menu-item:hover>a,
            .widget_search label:before,
            .post-content .post-title a,
            .mec-event-sharing-wrap li i,
            .mec-events-masonry-cats a.mec-masonry-cat-selected {
                color: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ); ?>;
            }

            .mec-event-sharing-wrap li i:hover {
                color: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ) . 'CC'; ?>;
            }

            .vacancies--item_content a, 
            .vacancies--item_content-light a {
                color: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ); ?>!important;
            }

            .header_nav .menu > .menu-item > a .menu-item-notify,
            .field.-on_dark input:focus, 
            .field.-on_dark textarea:focus,
            .field input:focus, 
            .field textarea:focus,
            .our_company--faq_title-letter, 
            .our_company--faq_text-letter,
            body .booked-list-view button.button,
            .mec-wrap .mec-totalcal-box .mec-totalcal-view span.mec-totalcalview-selected,
            .mec-month-divider span:before,
            .widget .tagcloud a:hover,
            .mec-events-masonry-cats a.mec-masonry-cat-selected {
                border-color: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ); ?>;
            }

            body .booked-list-view button.button:hover {
                border-color: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ); ?>!important; 
            }

            .service_item_hover:hover,
            .slick-slider .slick-dots>li button:before,
            .-on_dark .wpcf7-submit,
            .promo_slider_nav--item .line,
            .button.-red,
            .clients.-dark .slick-slider .slick-dots>li button:before,
            .button.-yellow,
            .button.-white_orange:hover,
            .service_item_hover_blue:hover,
            .button.-blue_light,
            .article.-blog_page>.post-cats a,
            .slick-slider .slick-dots>li button:before,
            .clients_reviews-sm .slick-slider .slick-dots button:before,
            .map--opener {
                background-color: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ); ?>;
            }

            .-on_dark .wpcf7-submit:hover,
            .button.-red:hover,
            .button.-yellow:hover,
            .blue_layer .case_studies--item:hover:after,
            .button.-blue_light:hover {
                background-color: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ) . 'CC'; ?>;
            }

            .mec-event-sharing-wrap:hover>li {
                background-color: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ) . '26'; ?>;
            }  

            .pricing_plan--label {
                background-color: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ); ?>!important;
            }

            .footer_contact_info--item i,
            .contact_mini--icon,
            #grid-1119 .tg-nav-color,
            .tg-nav-color {
                color: <?php echo esc_html( $meta_data[ 'custom_second_footer_color' ] ); ?>!important;
            }

        <?php }
    }
}

$tm_button_color        = chade_get_options( 'tm_button_color' );
$tm_button_hover_color  = chade_get_options( 'tm_button_hover_color' );
$tm_button_bg_color     = chade_get_options( 'tm_button_bg_color' );

if ( ! empty( $tm_button_color ) ) : ?>
.top-menu-button {
    color: <?php echo esc_html( $tm_button_color ); ?>!important;   
}
<?php endif;

if ( ! empty( $tm_button_hover_color ) ) : ?>
.top-menu-button:hover {
    background-color: <?php echo esc_html( $tm_button_hover_color ); ?>!important;   
}
<?php  endif;

if ( ! empty( $tm_button_bg_color ) ) : ?>
.top-menu-button {
    background-color: <?php echo esc_html( $tm_button_bg_color ); ?>!important;   
}
<?php endif;