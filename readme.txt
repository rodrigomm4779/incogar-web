Theme Name: Chade
Theme URI: https://chade.foxthemes.me/
Author: FOXTHEMES
Author URI: https://themeforest.net/user/fox-themes
Description: Chade is a WordPress theme created for construction companies, building firms, plumbing, heating, painting, tiling, refurbishment, partitions & ceilings, plastering & rendering, carpentry & joinery as well as architecture professionals.
Version: 1.1.4
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: two-columns, three-columns, left-sidebar, right-sidebar, custom-background, custom-header, custom-menu, editor-style, featured-images, flexible-header, full-width-template, microformats, post-formats, rtl-language-support, sticky-post, theme-options
Text Domain: chade

## Privacy Policy
Chade uses [Appsero](https://appsero.com) SDK to collect some telemetry data upon user's confirmation. This helps us to troubleshoot problems faster & make product improvements.

Appsero SDK **does not gather any data by default.** The SDK only starts gathering basic telemetry data **when a user allows it via the admin notice**. We collect the data to ensure a great user experience for all our users.

Integrating Appsero SDK **DOES NOT IMMEDIATELY** start gathering data, **without confirmation from users in any case.**

Learn more about how [Appsero collects and uses this data](https://appsero.com/privacy-policy/).