<?php
/**
 * Header template.
 *
 * @package chade
 * @since 1.0.0
 *
 */
// Get available languages for switcher.
$languages = chade_get_languges();

// Check if lagnguages exist and set needed css class.
$follow_us_class = $languages ? 'col-md-9' : 'col-md-12';

// Get header social.
$tm_social = chade_get_options( 'tm_social' );

// Site favicon.
$site_favicon = chade_get_options( 'site_favicon' );

/* Stiky header class */
$sticky_header_class = chade_get_options( 'sticky_header' ) ? 'header_sticky' : '';

$unitClass = ! function_exists( 'cs_framework_init' ) ? 'unit ' : ''; ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php if ( ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) && $site_favicon ) { ?>
        <link href="<?php echo esc_attr( $site_favicon ); ?>" rel="shortcut icon"/>
	<?php } ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php chade_preloader(); ?>
<div id="main" class="<?php echo esc_attr( $unitClass ); ?>">
    <div class="layout">
        <div class="layout--header">

			<?php if ( (  $tm_social && count( $tm_social ) ) > 0 || $languages ): ?>
                <!-- Additinal topbar for social icons, language swithcer and custom contac items -->
                <div class="topbar visible-md-block visible-lg-block">
                    <div class="container">
                        <div class="row">
							<?php if ( is_array( $languages ) && count( $languages ) > 0 ): ?>
                                <!-- Language switcher -->
                                <div class="col-md-3">
                                    <div class="topbar--left">
                                        <div class="select_language">
                                            <button type="button" class="select_language--opener">
                                                <i class="select_language--opener_icon icons8-globe-earth"></i><?php esc_html_e( 'Language', 'chade' ); ?>
                                            </button>
                                            <ul class="select_language--list">
												<?php foreach ( $languages as $l ) {
													$active_language = ! $l['active'] ? 'not-active' : 'active'; ?>
                                                    <li class="<?php echo esc_attr( $active_language ); ?>">
                                                        <a href="<?php echo esc_url( $l['url'] ); ?>"><?php echo esc_html( $l['translated_name'] ); ?></a>
                                                    </li>
												<?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
							<?php endif; ?>
                            <div class="<?php echo esc_attr( $follow_us_class ); ?>">

                                <!-- Topbar_contacts-->
                                <div class="topbar--right">

									<?php if ( $tm_social && count( $tm_social ) > 0 ): ?>
                                        <!-- Our Social -->
                                        <div class="follow_us">
                                            <strong><?php esc_html_e( 'Síguenos', 'chade' ); ?></strong>
                                            <ul>
												<?php foreach ( $tm_social as $social ) { ?>
                                                    <li>
                                                        <a href="<?php echo esc_url( $social['link'] ); ?>">
                                                            <i class="<?php echo esc_attr( $social['icon'] ); ?>"></i>
                                                        </a>
                                                    </li>
												<?php } ?>
                                            </ul>
                                        </div>
                                        <!-- End Social -->
									<?php endif; ?>

                                </div>
                                <!-- End topbar_contacts-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End topbbar -->
			<?php endif;
			// Our site header
			chade_top_header();
			if ( ! empty( $sticky_header_class ) && $sticky_header_class ) {
				// Add sticky header
				chade_top_header( $sticky_header_class );
			} ?>

        </div>