<?php
/**
 * The template includes necessary functions for theme.
 *
 * @package chade
 * @since 1.0.0
 *
 */

if ( ! isset( $content_width ) ) {
    $content_width = 960; /* pixels */
}

// Global variables for our theme.
// ------------------------------------------
defined( 'CHADE_URI' )    or define( 'CHADE_URI',    get_template_directory_uri() );
defined( 'CHADE_T_PATH' ) or define( 'CHADE_T_PATH', get_template_directory() );

// Helper functionality integration.
// ------------------------------------------
require_once CHADE_T_PATH . '/custom/inc.php';
require_once CHADE_T_PATH . '/custom/filters.php';
require CHADE_T_PATH . '/vendor/autoload.php';



/**
 * Initialize the plugin tracker
 *
 * @return void
 */
function appsero_init_tracker_chade() {

	if ( ! class_exists( 'Appsero\Client' ) ) {
		require_once CHADE_T_PATH . '/vendor/appsero/client/src/Client.php';
	}

	$client = new \Appsero\Client( 'b02fd7a5-d7e7-4d73-828b-0d2afa70bf7c', 'Chade', __FILE__ );

	// Active insights
	$client->insights()->init();

	// Active automatic updater
	$client->updater();

}

appsero_init_tracker_chade();



if( ! function_exists( 'chade_after_setup' ) ) {
    function chade_after_setup() {

        register_nav_menus(
            array(
                'primary-menu'    => esc_html__( 'Top menu', 'chade' ),
                'mobile-menu' => esc_html__( 'Mobile menu', 'chade' ),
            )
        );

        add_theme_support( 'post-formats', array('video', 'gallery', 'audio', 'quote'));
        add_theme_support( 'custom-header' );
        add_theme_support( 'custom-background' );
        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'title-tag' );
		remove_theme_support( 'widgets-block-editor' );

        add_image_size( 'chade-blog', 360, 220, true );
        add_image_size( 'chade-service-widget', 90, 90, true );
        add_image_size( 'chade-gallery-preview', 270, 184, true );
    }
    add_action( 'after_setup_theme', 'chade_after_setup' );
}

/*
 * Check minimal requirements (PHP and WordPress versions)
 */
if ( version_compare( $GLOBALS['wp_version'], '4.3', '<' ) || version_compare( PHP_VERSION, '5.3', '<' ) ) {
    function chade_requirements_notice() {
        $message = sprintf( esc_html__( 'chade theme needs minimal WordPress version 4.3 and PHP 5.3<br>You are running version WordPress - %s, PHP - %s.<br>Please upgrade need module and try again.', 'chade' ), $GLOBALS['wp_version'], PHP_VERSION );
        printf( '<div class="notice-warning notice"><p><strong>%s</strong></p></div>', $message );
    }
    add_action( 'admin_notices', 'chade_requirements_notice' );
}

if( ! function_exists( 'chade_fix_svg' ) ) {
	function chade_fix_svg() {
		echo '<style type="text/css">
          .attachment-266x266, .thumbnail img {
               width: 100% !important;
               height: auto !important;
          }
          </style>';
	}
}

add_action('admin_head', 'chade_fix_svg');

function chade_set_script( $scripts, $handle, $src, $deps = array(), $ver = false, $in_footer = false ) {
	$script = $scripts->query( $handle, 'registered' );
	if ( $script ) {
		// If already added
		$script->src  = $src;
		$script->deps = $deps;
		$script->ver  = $ver;
		$script->args = $in_footer;
		unset( $script->extra['group'] );
		if ( $in_footer ) {
			$script->add_data( 'group', 1 );
		}
	} else {
		// Add the script
		if ( $in_footer ) {
			$scripts->add( $handle, $src, $deps, $ver, 1 );
		} else {
			$scripts->add( $handle, $src, $deps, $ver );
		}
	}
}
function chade_replace_scripts( $scripts ) {
	$assets_url = CHADE_URI . '/assets/libs/';
	chade_set_script( $scripts, 'jquery-migrate', $assets_url . 'jquery-migrate.min.js', array(), '1.4.1-wp' );
	chade_set_script( $scripts, 'jquery', false, array( 'jquery-core', 'jquery-migrate' ), '1.12.4-wp' );
}
add_action( 'wp_default_scripts', 'chade_replace_scripts' );