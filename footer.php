<?php
/**
 * Footer template.
 *
 * @package chade
 * @since 1.0.0
 * @version 1.0.0
 */

// Footer options
$bg_image       = chade_get_options( 'footer_bg_image', CHADE_URI . '/assets/images/footer.jpg' );
$bg_image_attr  = $bg_image ? 'background-image: url(' . esc_url( $bg_image ) . ');' : 'background: transparent;';
$bg_color       = chade_get_options( 'footer_background_color', '#1b1f20' );
$copy_text      = chade_get_options( 'footer_copy', 'Chade Theme &copy;2017. All Rights Reserved.' );
$footer_logo    = chade_get_options( 'footer_logo', CHADE_URI . '/assets/images/logo-header.png' );
$footer_sidebar = chade_get_options( 'footer_sidebar' );

$footer_class = ( $footer_sidebar && is_active_sidebar( 'footer-sidebar' ) ) ? '' : 'hide-margin';

/* Mobile menu */
$languages           = chade_get_languges();
$mm_social           = chade_get_options( 'mm_social' );
$mm_button           = chade_get_options( 'mm_button' );
$mm_button_text      = chade_get_options( 'mm_button_text' );
$mm_button_link      = chade_get_options( 'mm_button_link' );
$mm_additional       = chade_get_options( 'mm_additional' );
$mm_background_color = chade_get_options( 'mm_background_color', '#ffffff' );
$mobile_background   = $mm_background_color ? 'background-color: ' . $mm_background_color . ';' : 'background-color:transparent;';
$unitClass           = ! function_exists( 'cs_framework_init' ) ? 'sidebar-unit ' : '';

/* Secondary footer */
$secondary_footer = chade_get_options( 'secondary_footer' );
$sf_items         = chade_get_options( 'sf_items' ); ?>

<?php if ( $secondary_footer && ! empty( $sf_items ) ): ?>
    <!-- Secondary footer -->
    <div class="layout--container">
        <div class="footer_contacts">
            <div class="section--inner container">
                <div class="row footer_contacts--flex">
                    <!-- Footer contact items -->
					<?php foreach ( $sf_items as $item ) { ?>
                        <div class="col-sm-4">
                            <div class="footer_contacts--item">
                                <div class="footer_contacts--item_inner">
									<?php if ( ! empty( $item['icon'] ) ): ?>
                                        <i class="<?php echo esc_attr( $item['icon'] ); ?>"></i>
									<?php endif;

                                    if ( ! empty( $item['title'] ) ) {
                                        if( !empty( $item['footer_link'] ) ) {
                                            $blank = $item['target_blank_footer'] ? 'target=_blank' : ''; ?>
                                            <a href="<?php echo esc_attr( $item['footer_link'] ); ?>" <?php echo esc_attr($blank); ?>>
                                                <b><?php echo esc_html( $item['title'] ); ?></b>
                                            </a>
                                        <?php } else { ?>
                                            <b><?php echo esc_html( $item['title'] ); ?></b>
                                        <?php }
                                    }
                                    if ( ! empty( $item['content'] ) ): ?>
                                        <p><?php echo esc_html( $item['content'] ); ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
					<?php } ?>
                    <!-- End footer contact items -->
                </div>
            </div>
        </div>
    </div>
    <!-- End secondary footer -->
<?php endif;

// View support widget.
chade_support_widget(); ?>

<!-- Main footer layout -->
<div class="layout--footer">
    <footer class="footer" style="<?php echo esc_attr( $bg_image_attr ); ?>">
        <div style="background-color:<?php echo esc_attr( $bg_color ); ?>" class="footer--bg"></div>
        <div class="footer--inner">
            <div class="container">
				<?php if ( $footer_sidebar && is_active_sidebar( 'footer-sidebar' ) ): ?>
                    <div class="footer_main">
                        <div class="row">
							<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer-sidebar' ) ) {
								;
							} ?>
                        </div>
                    </div>
				<?php endif;

				if ( $footer_logo || $copy_text ): ?>
                    <!-- Footer logo and copyright text -->
                    <div class="footer_copyrights <?php echo esc_attr( $footer_class ); ?>">
                        <div class="footer_copyrights--container">
                            <div class="row">
								<?php if ( $copy_text ): ?>
                                    <!-- Some copyright text area -->
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="footer_copyrights--item">
                                            <p class="footer_copyrights--item_copyrights">

												<?php if ( defined( 'ICL_LANGUAGE_CODE' ) && $copy_text['multilang'] == defined( 'ICL_LANGUAGE_CODE' ) ) {
													echo esc_html( $copy_text[0] );
												} else if ( is_string($copy_text) && ! empty( $copy_text ) ) {
                                                    echo esc_html( $copy_text );
                                                } else {
													echo esc_html( bloginfo( 'name' ) );
												} ?>
                                                
                                            </p>
                                        </div>
                                    </div>
                                    <!-- End coright text area -->
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- End footer logo and copyright text -->
				<?php endif; ?>
            </div>
        </div>
    </footer>
</div>
<!-- End main footer layout -->
</div>
</div>

<!-- Mobile navigation -->
<div id="mobile_sidebar"
     class="mobile_sidebar <?php echo esc_attr( $unitClass ); ?>" style="<?php echo esc_attr($mobile_background); ?>">
    <!-- Close navigation button -->
    <div class="mobile_sidebar--closer -white">
        <button class="c-hamburger c-hamburger--htx is-active">
            <span><?php esc_html_e( 'toggle menu', 'chade' ); ?></span></button>
    </div>
    <!-- End close navigation button -->

    <div class="mobile_menu">
		<?php chade_menu( 'mobile-menu' ); ?>
    </div>

	<?php if ( $languages ): ?>
        <!-- WPML language switcher -->
        <div class="select_language -mobile_sidebar">
            <button type="button" class="select_language--opener">
                <i class="select_language--opener_icon icons8-globe-earth"></i><?php esc_html_e( 'Language', 'chade' ); ?>
            </button>
            <ul class="select_language--list">
				<?php foreach ( $languages as $l ) {
					$active_language = ! $l['active'] ? 'not-active' : 'active'; ?>
                    <li class="<?php echo esc_attr( $active_language ); ?>">
                        <a href="<?php echo esc_url( $l['url'] ); ?>">
                            <?php echo esc_html( $l['translated_name'] ); ?>
                        </a>
                    </li>
				<?php } ?>
            </ul>
        </div>
        <!-- End WPML language switcher -->
	<?php endif;

	if ( $mm_additional && count( $mm_additional ) > 0 ): ?>
        <!-- Additional menu buttons -->
        <ul class="topbar_contacts -mobile_sidebar">
			<?php foreach ( $mm_additional as $item ) {
				$item_text = ! empty( $item['link'] ) ? '<a href="' . $item['link'] . '">' . $item['title'] . '</a>' : $item['title']; ?>
                <li class="topbar_contacts--item">
                    <span class="contact_phone">
                        <i class="<?php echo esc_attr( $item['icon'] ); ?>"></i>
                        <?php echo wp_kses_post( $item_text ); ?>
                    </span>
                </li>
			<?php } ?>
        </ul>
        <!-- End additional menu buttons -->
	<?php endif;

	if ( $mm_social && count( $mm_social ) > 0 ): ?>
        <!-- Social icons -->
        <div class="follow_us -mobile_sidebar">
            <ul>
				<?php foreach ( $mm_social as $social ) { ?>
                    <li>
                        <a href="<?php echo esc_url( $social['link'] ); ?>">
                            <i class="<?php echo esc_attr( $social['icon'] ); ?>"></i>
                        </a>
                    </li>
				<?php } ?>
            </ul>
        </div>
        <!-- End social icons -->
	<?php endif;

	if ( $mm_button ): ?>
        <!-- Custom button -->
        <div class="mobile_sidebar--buttons">
            <a href="<?php echo esc_url( $mm_button_link ); ?>" class="button -yellow">
                <?php echo esc_html( $mm_button_text ); ?>
            </a>
        </div>
        <!-- End custom button -->
	<?php endif ?>

</div>
<!-- End mobile navigation -->
<?php wp_footer(); ?>
</body>
</html>