<?php
/**
 * 404 Page temlate.
 *
 * @package chade
 * @since 1.0.0
 * @version 1.0.0
 */

$title    = chade_get_options( '404_title', '404 page' );
$subtitle = chade_get_options( '404_subtitle', 'Oops. looks like we got lost...' );
$text     = chade_get_options( '404_text' );
$btn_text = chade_get_options( '404_btn_text', 'Go Home' );
$btn_url  = chade_get_options( '404_btn_url', home_url( '/' ) );

get_header(); ?>

    <div class="layout--container page-404">
        <div class="page_header">
            <div class="container">
                <h1 class="page_header--title"><?php echo esc_html( $title ); ?></h1>
            </div>
        </div>
        <nav class="breadcrumbs">
            <div class="container">
                <ul class="breadcrumbs--list">
                    <li class="breadcrumbs--item">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="breadcrumbs--link">
                            <?php esc_html_e( 'Home', 'chade' ); ?>
                        </a>
                    </li>
                    <li class="breadcrumbs--item">
                        <a href="#" class="breadcrumbs--link -active"><?php echo esc_html( $title ); ?></a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="section">
            <div class="container">
                <div class="section--header">
                    <h2 class="section--title"><?php echo esc_html( $subtitle ); ?></h2>
					<?php if ( $text && ! empty( $text ) ): ?>
                        <div class="section--description">
							<?php echo wpautop( $text ); ?>
                        </div>
					<?php endif; ?>
                    <div class="link-wrapper">
                        <a href="<?php echo esc_url( $btn_url ); ?>" class="home-link">
                            <?php echo esc_html( $btn_text ); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>