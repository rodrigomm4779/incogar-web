<?php
/**
 * The main tamplate file.
 *
 * @package chade
 * @since 1.0.0
 *
 */
// Blog heading options.
$blog_heading = chade_get_options( 'blog_heading' );
$blog_heading_title = chade_get_options( 'blog_heading_title', 'Blog' );


$sidebar_head = chade_get_options( 'header_sidebar', false );
$center_class = ( ! $sidebar_head ) ? 'centered' : '';

// Sidebar.
$blog_sidebar = chade_get_options( 'blog_sidebar', true );

// Masonry style for blog posts.
$masonry_class = ( chade_get_options( 'masonry_blog' ) || !function_exists('cs_framework_init') ) ? 'masonry-blog' : '';

// Check if sidebar enable and put correct class for main content area.
$content_width_class = $blog_sidebar ? 'col-md-9 blog-sidebar' : 'col-md-12';

get_header(); ?>

    <div class="layout--container search-page">
		<?php if ( $blog_heading ): ?>
            <!-- Blog heading -->

			<?php if ( $blog_heading_title ): ?>
                <!-- Heading title -->
                <div class="page_header blog-heading <?php echo esc_attr( $center_class ); ?>">
                    <div class="container">
                        <h1 class="page_header--title"><?php echo esc_html( $blog_heading_title ); ?></h1>
                    </div>
                </div>
                <!-- End heading title -->
			<?php endif; ?>

            <!-- old breadcrumbs -->
            <!-- Breadcrumbs -->
            <nav class="breadcrumbs">
                <div class="container">
                    <ul class="breadcrumbs--list">
						<?php if ( is_home() && ! is_front_page() ): ?>
                            <li class="breadcrumbs--item">
                                <a href="<?php echo esc_url( home_url( '/' ) );?>" class="breadcrumbs--link">
									<?php esc_html_e( 'Home', 'chade' ); ?>
                                </a>
                            </li>
                            <li class="breadcrumbs--item">
                                <a href="#" class="breadcrumbs--link -active">
									<?php esc_html_e( 'Blog', 'chade' ); ?>
                                </a>
                            </li>
						<?php else: ?>
                            <li class="breadcrumbs--item">
                                <a href="<?php echo esc_url( home_url( '/' ) );?>" class="breadcrumbs--link -active">
									<?php esc_html_e( 'Home', 'chade' ); ?>
                                </a>
                            </li>
						<?php endif; ?>
                    </ul>
                </div>
            </nav>
            <!-- End breadcrumbs -->
            <!-- End blog heading -->
		<?php endif; ?>
        <div class="section">
            <div class="container">

                <div class="row">
                    <div class="<?php echo esc_attr( $content_width_class ); ?> col-xs-12">

						<?php if ( ! have_posts() ): ?>
                            <!-- Post not found -->
                            <div class="section--header search-section">
                                <p class="section--description">
									<?php esc_html_e('Sorry, no posts matched your criteria.', 'chade' ); ?>
                                </p>
                                <!-- Search form -->
								<?php get_search_form( true ); ?>
                                <!-- End search form -->
                            </div>
                            <!-- End post not found -->
						<?php else: ?>

                            <div class="section--container">
                                <div class="blog">
                                    <div class="row <?php echo esc_attr( $masonry_class ); ?>">

										<?php while ( have_posts() ): the_post();
											$post_title = get_the_title();
											$post_title = ! empty( $post_title ) ? $post_title : esc_html__( 'No title', 'chade' );
											$post_id  = get_the_ID();
											$post_categories = wp_get_post_categories( $post_id, $args = array() );
											?>
                                            <!-- Blog post -->
                                            <div <?php post_class( 'col-md-4 col-sm-6 blog-item' ); ?>>
                                                <div class="article -blog_page">

													<?php if ( has_post_thumbnail() ): ?>
                                                        <figure class="article--preview">
															<?php the_post_thumbnail( 'chade-blog' ); ?>
                                                        </figure>
													<?php endif; ?>

													<?php if ( $post_categories ) :
														$wio_image = ! has_post_thumbnail() ? 'wio_image' : ''; ?>
                                                        <div class="post-cats <?php echo esc_attr( $wio_image ); ?>">
															<?php foreach( $post_categories as $post_category ) :?>
                                                                <a href="<?php echo esc_attr( get_category_link( $post_category ) ); ?>"> <?php echo esc_html( get_category( $post_category )->name ); ?></a>
															<?php endforeach; ?>
                                                        </div>
													<?php endif; ?>

                                                    <div class="-blog_page-body">
                                                        <header class="article--header">
                                                            <h2 class="article--title">
                                                                <a href="<?php echo esc_url(get_the_permalink()); ?>">
																	<?php echo esc_html($post_title); ?>
                                                                </a>
                                                            </h2>

                                                        </header>

                                                        <div class="article--content">
															<?php the_excerpt(); ?>
                                                        </div>

                                                        <div class="article--footer">
                                                            <ul class="article--meta">
                                                                <li class="article--meta_item -date"><?php the_time( get_option('date_format') ); ?></li>
																<?php

																if(get_comments_number() > 0){ ?>
                                                                    <li class="article--meta_item -comments"><?php echo get_comments_number(); ?></li>
																<?php }?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End blog post -->

										<?php endwhile; ?>

                                    </div>

									<?php if ( $paginate_links = paginate_links( array( 'type' => 'plain', 'prev_text' => '<i class="icons8-right"></i>&nbsp; ' . esc_html__( 'Previous Page', 'chade'), 'next_text' => esc_html__( 'Next Page', 'chade') . ' &nbsp;<i class="icons8-right"></i>' ) ) ): ?>
                                        <!-- Blog pagination -->
                                        <nav class="pagination blog-navigation">
                                            <div class="pagintion-wrapper">
												<?php echo wp_kses_post( $paginate_links ); ?>
                                            </div>
                                        </nav>
                                        <!-- End blog pagination -->
									<?php endif; ?>
                                </div>
                            </div>
						<?php endif; ?>
                    </div>

					<?php if ( $blog_sidebar ): ?>
                        <!-- Sidebar area -->
                        <div class="col-md-3 col-xs-12 blog-sidebar">
                            <div class="blog_sidebar">
								<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar('sidebar') ); ?>
                            </div>
                        </div>
                        <!-- End sidebar area -->
					<?php endif; ?>

                </div>
            </div>

        </div>
    </div>
    </div>

<?php get_footer(); ?>