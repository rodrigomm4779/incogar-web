<?php
/**
 * chade Comment Form
 *
 * @package chade
 * @since 1.0.0
 * @version 1.0.0
 */

if ( post_password_required() ) { return; }

if ( have_comments() || comments_open() ) { ?>
	<div class="comments">
		<?php if ( have_comments() ) : ?>
			<h2 class="comments--title"><?php esc_html_e( 'Comments', 'chade' ); ?> (<?php echo get_comments_number(); ?>)</h2>
			
			<!-- Comments list -->
			<ul class="comments_list">

				<?php wp_list_comments( array( 'callback' => 'chade_comment' ) ); ?>

				<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
					<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
						<div class="nav-previous">
                            <?php previous_comments_link( esc_html__( '&larr; Older Comments', 'chade' ) ); ?>
                        </div>
						<div class="nav-next">
                            <?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'chade' ) ); ?>
                        </div>
					</nav>
				<?php endif; ?>

			</ul>
			<!-- End comments list -->
		<?php endif; // have_comments() ?>

		<?php if ( comments_open() ) : ?>
			<!-- Comments form -->
			<div class="comment_form form">
				<h2 class="comment_form--title"><?php esc_html_e( 'Leave Comment', 'chade' ); ?></h2>
				<?php
					comment_form(
						array(
							'id_form'              => 'comment-form',
							'fields'               => array(
								'author'            => '<div class="form--row">
															<div class="form--row_inner">
																<div class="form--col">
																	<label class="field -wide">
																		<input type="text" name="author" placeholder="' . esc_attr__( 'Full Name', 'chade') . '" required />
																	</label>
																</div>',
								'email'             => '		<div class="form--col">
																	<label class="field -wide">
																		<input name="email" type="text" placeholder="' . esc_attr__( 'Email', 'chade') . '" required />
																	</label>
																</div>',
								'website'           => '		<div class="form--col">
																	<label class="field -wide">
																		<input type="text" name="website" placeholder="' . esc_attr__( 'Website URL', 'chade') . '">
																	</label>
																</div>
															</div>
														</div>',
							),
							'comment_field'        => ' <div class="form--row">
															<div class="form--row_inner">
																<div class="form--col">
																	<label class="field -wide">
																		<textarea name="comment" cols="30" rows="10" placeholder="' . esc_attr__( 'Type in here...', 'chade') . '" required></textarea>
																	</label>
																</div>
															</div>
														</div>',
							'must_log_in'          => '',
							'logged_in_as'         => '',
							'comment_notes_before' => '',
							'comment_notes_after'  => '',
							'title_reply'          => '',
							'title_reply_to'       => esc_html__( 'Leave a Reply to %s', 'chade' ),
							'cancel_reply_link'    => esc_html__( 'Cancel', 'chade' ),
							'label_submit'         => esc_html__( 'Publish Comment', 'chade' ),
							'submit_button'        => '<div class="comment_form--footer form--footer">
															<div class="form--footer_buttons">
																<button name="%1$s" type="submit" id="%2$s" class="%3$s button -yellow -bordered"><span class="button--inner">%4$s</span></button>
															</div>
														</div>',
							'submit_field'         => '%1$s %2$s',
						)
					);
				?>	
			</div>
		<?php endif; ?>
		<!-- End comments form -->
	</div>
<?php } ?>