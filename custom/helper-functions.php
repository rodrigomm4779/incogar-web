<?php
/**
 * Helper functions file.
 *
 * @package chade
 * @since 1.0.0
 *
 */

/**
 *
 * View Site Logo.
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( ! function_exists( 'chade_get_site_logo' ) ) {
    function chade_get_site_logo( $sticky = '' ) {
        $image_logo_width = chade_get_options('image_logo_width', '');
        $width = '';

        if( ! empty( $image_logo_width ) ) {
            $width = is_numeric( $image_logo_width ) ? $image_logo_width . 'px' : $image_logo_width;
            $width = 'style="width:' . esc_attr($width) . ';"';
        }

        /* Sticky header logo */
        if( ! empty( $sticky ) ) {
            $sticky_logo_type = chade_get_options('sticky_logo_type');
            if( $sticky_logo_type && $sticky_logo_type == 'new' ) {
                $sticky_logo = chade_get_options( 'sticky_logo' );
                if( $sticky_logo ) {
                    echo '<img src="' . esc_url( $sticky_logo ) . '" ' . $width . ' alt="' . esc_attr(get_bloginfo('name')) . '">';
                    return;
                }
            }
        }

        /* Main header logo */
        $logo_type = chade_get_options('logo_type', 'image');
        if( $logo_type == 'image' ) {
            $image_logo = chade_get_options('image_logo', get_template_directory_uri() . '/assets/images/logo-header.png' );
            echo '<img src="' . esc_url( $image_logo ) . '" ' . $width . ' alt="' . esc_attr(get_bloginfo('name')) . '">';
        }
        else {
            $text_logo = chade_get_options('text_logo', 'Chade');
            echo esc_html( $text_logo );
        }
    }
}

/**
 *
 * Get Theme Option.
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( ! function_exists( 'chade_get_options' ) ) {
    function chade_get_options( $option, $default = false ) {
        if( function_exists( 'cs_framework_init' ) ) {
            global $chade_options;

            if( empty( $chade_options ) ) {
                $chade_options = apply_filters( 'cs_get_option', get_option( CS_OPTION ) );
            }

            return isset( $chade_options[ $option ] ) ? $chade_options[ $option ] : $default;

        } else {
            return $default;
        }
    }
}

/* Check if WPML is activeted and return list of languges if its count > 1 */
if ( ! function_exists( 'chade_get_languges' ) ) {
    function chade_get_languges() {
        if( function_exists( 'icl_get_languages' ) ) {
            $languages = icl_get_languages('skip_missing=0');
            if( count( $languages ) > 1 ) {
                return $languages;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

/**
 * Get location menu.
 */
if ( ! function_exists( 'chade_menu' ) ) {
    function chade_menu( $location ) {
        if ( has_nav_menu( $location ) ) {

            $args = array(
                'container'      => '',
                'items_wrap'     => '<ul class="menu">%3$s</ul>',
                'theme_location' => $location,
                'depth'          => 4,
                'fallback_cb'    => '__return_empty_string'
            );

            if ( $location == 'primary-menu' ) {
                $walker = new Chade_Menu_Walker;
                $args['walker']	= $walker;
            }

            wp_nav_menu( $args );

        } else {
            echo '<div class="no-menu">' . esc_html__( 'Please register Navigation Menu from', 'chade' ) . ' <a href="' . esc_url( admin_url( 'nav-menus.php' ) ) . '" target="_blank">' . esc_html__( 'Appearance &gt; Menus', 'chade' ) . '</a></div>';
        }
    }
}

/**
 *
 * Add search buttom in top menu.
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( ! function_exists( 'chade_search_menu_button' ) ) {
    // Filter wp_nav_menu() to add additional links and other output
    function chade_search_menu_button( $items, $args ) {
        // Use only for top-menu location.
        if( ! empty( $args->theme_location ) && $args->theme_location == 'primary-menu' ) {
            $header_search_button = chade_get_options( 'header_search_button', true );

            if( $header_search_button ) {
                $search_link = '<li class="menu-item menu-item-search">
					<form action="' . esc_url( home_url( '/' ) ) . '" method="get" class="header_search js-header_search">
						<button type="button" class="header_search--opener js-header_search--opener"><i class="icons8-search"></i></button>
						<fieldset class="header_search--dropdown js-header_search--dropdown">
							<input type="text" placeholder="' . esc_attr__( 'SEARCH', 'chade' ) .'" value="' . get_search_query() . '" name="s" />
						</fieldset>
					</form>
				</li>';
                // add the search button to the end of the menu
                $items = $items . $search_link;
            }
        }
        return $items;
    }
    add_filter( 'wp_nav_menu_items', 'chade_search_menu_button', 10, 2 );
}

/* Return array of rigistered post types */
if ( ! function_exists( 'chade_get_post_types' ) ) {
    function chade_get_post_types( $ignore = array( 'revision', 'nav_menu_item', 'wpcf7_contact_form', 'testimonials', 'vc_grid_item' ) ) {

        $post_types = get_post_types();

        $cleaned = array_diff($post_types, $ignore);

        return $cleaned;
    }
}

/* helper function for HTML dropdowns */
if ( ! function_exists( 'chade_selected' ) ) {
    function chade_selected( $val1, $val2 = '' ) {
        if( $val1 == $val2 ) {
            echo 'selected';
        }
    }
}


/**
 *
 * Comments template.
 * @since 1.0.0
 * @version 1.0.0
 *
 **/
if ( ! function_exists( 'chade_comment' ) ) {
    function chade_comment( $comment, $args, $depth ) {

        $GLOBALS['comment'] = $comment;

        switch ( $comment->comment_type ):
            case 'pingback':
            case 'trackback': ?>
                <div class="pingback">
                    <?php esc_html_e( 'Pingback:', 'chade' ); ?> <?php comment_author_link(); ?>
                    <?php edit_comment_link( esc_html__( '(Edit)', 'chade' ), '<span class="edit-link">', '</span>' ); ?>
                </div>
                <?php
                break;
            default:
                // generate comments
                ?>
                <li <?php comment_class('comment'); ?> id="comment-<?php comment_ID(); ?>">
                <header class="comment--header">
                    <figure class="comment--userpic">
                        <?php echo get_avatar( $comment, 60 ); ?>
                    </figure>
                    <strong class="comment--username"><a href="#"><?php comment_author(); ?></a></strong>
                    <time datetime="2001-05-15T19:00" class="comment--date"><?php comment_date( get_option('date_format') );?></time>
                </header>
                <div class="comment--content">
                    <div class="article--content">
                        <?php comment_text(); ?>
                    </div>
                    <?php
                    comment_reply_link(
                        array_merge( $args,
                            array(
                                'reply_text' => esc_html__( 'Reply', 'chade' ),
                                'after' 	 => '',
                                'depth' 	 => $depth,
                                'max_depth'  => $args['max_depth']
                            )
                        )
                    );
                    ?>
                </div>
                <?php
                break;
        endswitch;
    }
}

/**
 * Get post format.
 */
if ( ! function_exists( 'chade_get_post_format' ) ) {
    function chade_get_post_format() {
        return get_post_format();
    }
}


/**
 *
 * Support widget function.
 * @since 1.0.0
 * @version 1.0.0
 *
 **/
if ( ! function_exists( 'chade_support_widget' ) ) {
    function chade_support_widget() {
        $widget = chade_get_options('help_widget', 'hide');
        if ( $widget != 'hide' ) {

            $help_title 		   = chade_get_options('help_title');
            $help_text 			   = chade_get_options('help_text');
            $help_page_url 		   = chade_get_options('help_page_url');
            $help_button_title 	   = chade_get_options('help_button_title');
            $help_icon 			   = chade_get_options('help_icon');
            $help_suppotr_image    = chade_get_options('help_suppotr_image');
            $help_suppotr_name 	   = chade_get_options('help_suppotr_name');
            $help_suppotr_position = chade_get_options('help_suppotr_position');

            $js_ind = ( $widget == 'open' ) ? 'js-tip' : '';
            $output = '';

            $widget_body  = '<div class="tip_help ' . esc_attr( $js_ind ) . '">';
            $widget_body .= ( $help_title ) ? '<h2 class="tip_help--title">' . esc_html( $help_title ) . '</h2>' : '';
            $widget_body .= ( $help_text ) ? '<p class="tip_help--text">' . esc_html( $help_text ) . '</p>' : '';
            $widget_body .= ( $widget == 'open' ) ? '<button type="button" class="tip_help--close js-tip--close"><span></span></button>' : '';
            $widget_body .= '<div class="tip_help--contact">';
            $widget_body .= '<div class="tip_help--contact_card">';

            if( $help_suppotr_image ) {
                $widget_body .= '<figure class="tip_help--contact_image">';
                $widget_body .= '<img src="' . esc_url( $help_suppotr_image ) . '" alt="'. esc_attr__('Help support', 'chade') .'">';
                $widget_body .= '</figure>';
            }

            $widget_body .= ( $help_suppotr_name ) ? '<a href="#" class="tip_help--contact_name">' . esc_html( $help_suppotr_name ) . '</a>' : '';
            $widget_body .= ( $help_suppotr_position ) ? '<small class="tip_help--contact_job">' . esc_html( $help_suppotr_position ) . '</small>' : '';
            $widget_body .= '</div>';

            if( $help_button_title && $help_page_url ) {
                $widget_body .= '<div class="tip_help--contact_button">';
                $widget_body .= '<a href="' . esc_url( $help_page_url ) . '" class="button -blue_light -bordered -menu_size"><span class="button--inner">' . esc_html( $help_button_title ) . '</span></a>';
                $widget_body .= '</div>';
            }

            $widget_body .= '</div>';
            $widget_body .= '</div>';


            if( $widget == 'open' ) {
                $output = $widget_body;
            } else {
                $icon = ( $help_icon && ! empty( $help_icon ) ) ? $help_icon : 'icons8-talk';
                $output .= '<div class="support_chat js-support_chat">';
                $output .= '<button type="button" class="support_chat--opener js-support_chat--opener">';
                $output .= '<i class="' . esc_attr( $help_icon ) . '"></i>';
                $output .= '</button>';
                $output .= '<div class="support_chat--window js-support_chat--window">';
                $output .= '<div class="support_chat--container">';
                $output .= $widget_body;
                $output .= '</div>';
                $output .= '</div>';
                $output .= '</div>';
            }
            echo wp_kses_post( $output );
        }
    }
}

/**
 *
 * Get site nav menu.
 * @since 1.0.0
 * @version 1.0.0
 *
 **/
if ( ! function_exists( 'chade_top_header' ) ) {
    function chade_top_header( $sticky = '' ) {
        /* Custom button */
        $tm_button 		= chade_get_options( 'tm_button' );
        $tm_button_text = chade_get_options( 'tm_button_text' );
        $tm_button_link = chade_get_options( 'tm_button_link' );
    
        /* Header Sidebar */
        $header_sidebar = chade_get_options( 'header_sidebar', false );
        $h_class = ( ! $header_sidebar ) ? '-activate' : '';

        /* Transparent */
        $transparent_header = chade_get_options( 'transparent_header', false );

        /* Remove right padding for sidebar if button not set */
        $padding_class = $tm_button ? '' : 'remove-padding';
        $header_class = "";


        if ( $transparent_header ) {
            $header_class = "transparent";
        } ?>
        <header class="header <?php echo esc_attr( $sticky ) . " " . esc_attr( $header_class ); ?>">
            <div class="container">
                <div class="header--inner">
                    <div class="row">
                        <div class="col-lg-2 col-md-12">
                            <div class="header--logo logo">
                                <a href="<?php echo esc_url( home_url( '/' ) );?>">
                                    <?php chade_get_site_logo( $sticky ); ?>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-12 visible-md-block visible-lg-block">
                            <div class="header--right <?php echo esc_attr( $h_class ); ?>">

                                <?php if ( $header_sidebar && is_active_sidebar( 'header-sidebar' ) ): ?>
                                    <div class="header_contacts <?php echo esc_attr( $padding_class ); ?>">
                                        <?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar('header-sidebar') ); ?>
                                    </div>
                                <?php elseif ( $header_sidebar ): ?>
                                <div class="header_contacts <?php echo esc_attr( $padding_class ); ?>">
                                    <?php echo chade_header_contacts(); ?>
                                </div>
                                <?php else: ?>
                                    <nav class="header_nav -on_dark">
                                        <div class="header_nav--inner">
                                            <!-- Main menu -->
                                            <?php chade_menu( 'primary-menu' ); ?>
                                        </div>
                                    </nav>
                                <?php endif; ?>

                                <?php if ( $tm_button ):
                                    $button_arrow = chade_get_options( 'tm_button_arrow' ) ? '-arrowed' : ''; ?>
                                    <a href="<?php echo esc_url( $tm_button_link ); ?>" class="button -yellow <?php echo esc_attr( $button_arrow ); ?> -menu_size top-menu-button">
                                            <span class="button--inner"><?php echo esc_html( $tm_button_text ); ?></span>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ( ( $header_sidebar && is_active_sidebar( 'header-sidebar' ) ) || $header_sidebar ): ?>
                <nav class="header_nav -wide visible-md-block visible-lg-block">
                    <div class="header_nav--inner">
                        <!-- Main menu -->
                        <?php chade_menu( 'primary-menu' ); ?>
                    </div>
                </nav>
            <?php endif; ?>

            <div class="header--menu_opener visible-xs-block visible-sm-block">
				<span class="c-hamburger c-hamburger--htx">
					<span><?php esc_html_e( 'toggle menu', 'chade' ); ?></span>
				</span>
            </div>
        </header>
    <?php }
}

/**
 * displays contact information
 */
if ( ! function_exists( 'chade_header_contacts' ) ) {
    function chade_header_contacts() {

	    $header_sidebar = chade_get_options( 'header_sidebar', false );
	    $html = '';


	    if ( $header_sidebar ) {
		    $tm_contacts = chade_get_options( 'tm_contacts' );

		    foreach ( $tm_contacts as $tm_contact ) {

		        if(isset( $tm_contact['target_blank'] ) && !empty( $tm_contact['target_blank'] )) {
		            $target_blank = 'target="_blank"';
                } else {
                    $target_blank = '';
                }

			    $html .= '<div class="chade-widget-wrap">';
			        $html .= '<div class="header_contacts--item">';
			            $html .= '<div class="contact_mini">';
                            $html .= '<i style="color:#ff5e14;" class="contact_mini--icon ' . $tm_contact['icon'] . '"></i>';
			                $html .= '<h6><a href="' . $tm_contact['link'] . '" ' . $target_blank . '>' . $tm_contact['title'] . '</a></h6>';
			            $html .= '</div>';
			        $html .= '</div>';
			    $html .= '</div>';

		    }
	    }

	    return $html;
    }
}

/**
 *
 * View page preloader.
 * @since 1.0.0
 * @version 1.0.0
 *
 **/
if ( ! function_exists( 'chade_preloader' ) ) {
    function chade_preloader() {
        $preloader = chade_get_options( 'preloader', true );
        if ( $preloader ) {
            $preloader_style = chade_get_options( 'preloader_style', 'spinner1' );
            $spinners = array(
                'spinner1' => '<div class="fl spinner1"><div class="double-bounce1"></div><div class="double-bounce2"></div></div>',
                'spinner2' => '<div class="fl spinner2"><div class="spinner-container container1"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container2"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container3"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div></div>',
                'spinner3' => '<div class="fl spinner3"><div class="dot1"></div><div class="dot2"></div></div>',
                'spinner4' => '<div class="fl spinner4"></div>',
                'spinner5' => '<div class="fl spinner5"><div class="cube1"></div><div class="cube2"></div></div>',
                'spinner6' => '<div class="fl spinner6"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>',
                'spinner7' => '<div class="fl spinner7"><div class="circ1"></div><div class="circ2"></div><div class="circ3"></div><div class="circ4"></div></div>'
            );
            ?>
            <div id="preloader">
                <?php echo wp_kses_post( $spinners[ $preloader_style ] ); ?>
            </div>
        <?php }
    }
}

/**
 *
 * Remove letters from string.
 * @since 1.0.0
 * @version 1.0.0
 *
 **/
if ( ! function_exists( 'chade_get_number_from_string' ) ) {
    function chade_get_number_from_string( $str ){
        $number = preg_replace("/[^0-9|\.]/", '', $str);
        return $number;
    }
}

/**
 *
 * Remove numbers from string.
 * @since 1.0.0
 * @version 1.0.0
 *
 **/
if ( ! function_exists( 'chade_get_str_from_string' ) ) {
    function chade_get_str_from_string( $str ){
        $strig = preg_replace("/[0-9|\.]/", '', $str);
        return $strig;
    }
}


remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

if ( ! function_exists( 'chade_remove_wc_breadcrumbs' ) ) {
	function chade_remove_wc_breadcrumbs() {
		remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
	}
}
add_action( 'init', 'chade_remove_wc_breadcrumbs' );

if ( ! function_exists( 'chade_breadcrumbs' ) ) {
	function chade_breadcrumbs() {
		global $post;

		$text['home']     = 'Home';
		$text['category'] = '%s';
		$text['search']   = 'Search page';
		$text['tag']      = 'Tag "%s"';
		$text['author']   = '%s';
		$text['404']      = 'Error 404';
		$text['page']     = '%s';
		$text['cpage']    = '%s';

		$wrap_before       = '<nav class="breadcrumbs">';
		$wrap_after        = '</nav>';
		$container         = '<div class="container"><ul class="breadcrumbs--list">';
		$after_container   = '</ul></div>';
		$sep               = '';
		$sep_before        = '';
		$sep_after         = '';
		$show_home_link    = 1;
		$show_on_home      = 0;
		$show_current      = 1;
		$before            = '<li class="breadcrumbs--item">';
		$after             = '</li>';
		$active_link       = '<span class="breadcrumbs--link -active">';
		$after_active_link = '</span>';

		$home_url    = home_url( '/' );
		$link_before = '<li class="breadcrumbs--item">';
		$link_after  = '</li>';
		$link_in_before = '<span>';
		$link_in_after  = '</span>';
		$link           = $link_before . '<a href="%1$s" class="breadcrumbs--link">' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
		$frontpage_id   = get_option( 'page_on_front' );
		$parent_id      = ( $post ) ? $post->post_parent : '';
		$sep            = ' ' . $sep_before . $sep . $sep_after . ' ';
		$home_link      = $link_before . '<a href="' . esc_url($home_url) . '" class="breadcrumbs--link">' . $link_in_before . $text['home'] . $link_in_after . '</a>' . $link_after;

		if ( is_home() || is_front_page() ) {

			if ( $show_on_home ) {
				echo wp_kses_post( $wrap_before . $container . $home_link . $after_container . $wrap_after );
			}

		} else {

			echo wp_kses_post( $wrap_before . $container );
			if ( $show_home_link ) {
				echo wp_kses_post( $home_link );
			}

			if ( is_category() ) {
				$cat = get_category( get_query_var( 'cat' ), false );
				if ( $cat->parent != 0 ) {
					$cats = get_category_parents( $cat->parent, true, $sep );
					$cats = preg_replace( "#^(.+)$sep$#", "$1", $cats );
					if ( $show_home_link ) {
						echo wp_kses_post( $sep );
					}
					echo wp_kses_post( $cats );
				}
				if ( get_query_var( 'paged' ) ) {
					$cat = $cat->cat_ID;
					echo wp_kses_post( $sep . sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ) ) . $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after );
				} else {
					if ( $show_current ) {
						echo wp_kses_post( $sep . $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after );
					}
				}

			} elseif ( is_search() ) {
				$search_link = '<a href="' . esc_url(get_search_link()) . '" class="breadcrumbs--link -active">' . esc_html__('Search page', 'chade') . '</a>';
				if ( $show_current ) {
					echo wp_kses_post( $before . $search_link . $after );
				}
			} elseif ( is_day() ) {
				if ( $show_home_link ) {
					echo wp_kses_post( $sep );
				}
				echo wp_kses_post( sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $sep );
				echo wp_kses_post( sprintf( $link, get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ), get_the_time( 'F' ) ) );
				if ( $show_current ) {
					echo wp_kses_post( $sep . $before . get_the_time( 'd' ) . $after );
				}

			} elseif ( is_month() ) {
				if ( $show_home_link ) {
					echo wp_kses_post( $sep );
				}
				echo wp_kses_post( sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) );
				if ( $show_current ) {
					echo wp_kses_post( $sep . $before . get_the_time( 'F' ) . $after );
				}

			} elseif ( is_year() ) {
				if ( $show_home_link && $show_current ) {
					echo wp_kses_post( $sep );
				}
				if ( $show_current ) {
					echo wp_kses_post( $before . get_the_time( 'Y' ) . $after );
				}

			} elseif ( is_single() && ! is_attachment() ) {
				if ( $show_home_link ) {
					echo wp_kses_post( $sep );
				}
				if ( get_post_type() != 'post' ) {

					$post_type = get_post_type_object( get_post_type() );

					$pages = array( 'services' => 'service' );

					$slug = $post_type->rewrite;
					printf( $link, $home_url . $pages[ $slug['slug'] ] . '/', $post_type->labels->name );
					if ( $show_current ) {
						echo wp_kses_post( $sep . $before . $active_link . get_the_title() . $after_active_link . $after );
					}
				} else {
					$cat  = get_the_category();
					$cat  = $cat[0];
					$cats = get_category_parents( $cat, true, $sep );
					if ( ! $show_current || get_query_var( 'cpage' ) ) {
						$cats = preg_replace( "#^(.+)$sep$#", "$1", $cats );
					}
					$cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . ' class="breadcrumbs--link">' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats );
					echo wp_kses_post( $cats );
					if ( get_query_var( 'cpage' ) ) {
						echo wp_kses_post( $sep . sprintf( $link, get_permalink(), get_the_title() ) . $sep . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after );
					} else {
						if ( $show_current ) {
							echo wp_kses_post( $before . get_the_title() . $after );
						}
					}
				}

				// custom post type
			} elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' && ! is_404() ) {
				$post_type = get_post_type_object( get_post_type() );
				if ( get_query_var( 'paged' ) ) {
					echo wp_kses_post( $sep . sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->label ) . $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after );
				} else {
					if ( $show_current ) {
						echo wp_kses_post( $sep . $before . $post_type->label . $after );
					}
				}

			} elseif ( is_attachment() ) {
				if ( $show_home_link ) {
					echo wp_kses_post( $sep );
				}
				$parent = get_post( $parent_id );
				$cat    = get_the_category( $parent->ID );
				$cat    = $cat[0];
				if ( $cat ) {
					$cats = get_category_parents( $cat, true, $sep );
					$cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . ' class="breadcrumbs--link">' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats );
					echo wp_kses_post( $cats );
				}
				printf( $link, get_permalink( $parent ), $parent->post_title );
				if ( $show_current ) {
					echo wp_kses_post( $sep . $before . get_the_title() . $after );
				}

			} elseif ( is_page() && ! $parent_id ) {
				if ( $show_current ) {
					echo wp_kses_post( $sep . $before . $active_link . get_the_title() . $after_active_link . $after );
				}

			} elseif ( is_page() && $parent_id ) {
				if ( $show_home_link ) {
					echo wp_kses_post( $sep );
				}
				if ( $parent_id != $frontpage_id ) {
					$breadcrumbs = array();
					while ( $parent_id ) {
						$page = get_page( $parent_id );
						if ( $parent_id != $frontpage_id ) {
							$breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ), get_the_title( $page->ID ) );
						}
						$parent_id = $page->post_parent;
					}
					$breadcrumbs = array_reverse( $breadcrumbs );
					for ( $i = 0; $i < count( $breadcrumbs ); $i ++ ) {
						echo wp_kses_post( $breadcrumbs[ $i ] );
						if ( $i != count( $breadcrumbs ) - 1 ) {
							echo wp_kses_post( $sep );
						}
					}
				}
				if ( $show_current ) {
					echo wp_kses_post( $sep . $before . $active_link . get_the_title() . $after_active_link . $after );
				}
			} elseif ( is_tag() ) {
				if ( get_query_var( 'paged' ) ) {
					$tag_id = get_queried_object_id();
					$tag    = get_tag( $tag_id );
					echo wp_kses_post( $sep . sprintf( $link, get_tag_link( $tag_id ), $tag->name ) . $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after );
				} else {
					if ( $show_current ) {
						echo wp_kses_post( $sep . $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after );
					}
				}

			} elseif ( is_author() ) {
				global $author;
				$author = get_userdata( $author );
				if ( get_query_var( 'paged' ) ) {
					if ( $show_home_link ) {
						echo wp_kses_post( $sep );
					}
					echo wp_kses_post( sprintf( $link, get_author_posts_url( $author->ID ), $author->display_name ) . $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after );
				} else {
					if ( $show_home_link && $show_current ) {
						echo wp_kses_post( $sep );
					}
					if ( $show_current ) {
						echo wp_kses_post( $before . sprintf( $text['author'], $author->display_name ) . $after );
					}
				}

			} elseif ( is_404() ) {
				if ( $show_home_link && $show_current ) {
					echo wp_kses_post( $sep );
				}
				if ( $show_current ) {
					echo wp_kses_post( $before . $text['404'] . $after );
				}

			} elseif ( has_post_format() && ! is_singular() ) {
				if ( $show_home_link ) {
					echo wp_kses_post( $sep );
				}
				echo wp_kses_post( get_post_format_string( get_post_format() ) );
			}

			echo wp_kses_post( $after_container . $wrap_after );

		}
	}
}
/*
 * Blog item header.
 */
if ( ! function_exists( 'chade_blog_item_hedeader' ) ) {

    function chade_blog_item_hedeader( $option, $post_id, $video_params = array(), $classButton = '', $classWrap = '' ) {
        $format = get_post_format( $post_id );
        if ( isset( $option[0]['post_preview_style'] ) ) {
            switch ( $option[0]['post_preview_style'] ) {
                case 'image':
                    $image     = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' );
                    $blog_type = cs_get_option( 'blog_type' ) ? cs_get_option( 'blog_type' ) : 'masonry';
                    $blog_type = apply_filters( 'chade_blog_style', $blog_type );
                    $imgClass  = $blog_type == 'masonry' ? '' : 's-img-switch';
                    $output    = '';
                    if ( ! empty( $image ) && ( $format != 'quote' ) ) {
                        $output .= '<div class="post-media">';
                        $output .= chade_the_lazy_load_flter( $image[0], array(
                            'class' => $imgClass,
                            'alt'   => ''
                        ) );
                        $output .= '</div>';
                    }

                    break;
                case 'video':
                    $output    = '<div class="post-media video-container iframe-video youtube ' . esc_attr( $classWrap ) . '" data-type-start="click">';
                    $video_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' );
                    $output    .= chade_the_lazy_load_flter( $video_img[0], array(
                        'class' => 's-img-switch',
                        'alt'   => ''
                    ) );

                    $video_link = $option[0]['post_preview_video'];

                    $output .= '<div class="video-content video-content-blog"><a href="' . esc_url( $video_link ) . '" class="play ' . esc_attr( $classButton ) . '"></a></div>';
                    $output .= '<span class="close fa fa-close"></span>';
                    $output .= '</div>';
                    break;
                case 'slider':
                    $output = '<div class="post-media">';
                    $output .= '<div class="img-slider">';
                    $output .= '<ul class="slides">';
                    $images = explode( ',', $option[0]['post_preview_slider'] );
                    foreach ( $images as $image ) {
                        $url = ( is_numeric( $image ) && ! empty( $image ) ) ? wp_get_attachment_url( $image ) : '';
                        if ( ! empty( $url ) ) {
                            $output .= '<li class="post-slider-img">';
                            $output .= chade_the_lazy_load_flter( $url, array(
                                'class' => 's-img-switch',
                                'alt'   => ''
                            ) );
                            $output .= '</li>';
                        }
                    }
                    $output .= '</ul>';
                    $output .= '</div>';
                    $output .= '</div>';

                    break;
                case 'text':
                    $output = '<i class="ion-quote"></i><blockquote>';
                    $output .= wp_kses_post( $option[0]['post_preview_text'] );
                    $output .= '</blockquote>';
                    if ( isset( $option[0]['post_preview_author'] ) && ! empty( $option[0]['post_preview_author'] ) ) {
                        $output .= '<cite>';
                        $output .= wp_kses_post( $option[0]['post_preview_author'] );
                        $output .= '</cite>';
                    }
                    break;
                case 'audio':
                    $output = '<div class="post-media">';
                    $output .= chade_post_media( $option[0]['post_preview_audio'] );
                    $output .= '</div>';
                    break;
                case 'link':
                    $output = '<div class="link-wrap"><i class="icon-link-3"></i><a href="' . esc_url( $option[0]['post_preview_link'] ) . '">';
                    $output .= wp_kses_post( $option[0]['post_preview_link'] );
                    $output .= '</a></div>';
                    break;
            }
        } else {
            $output = '';
            if ( $format == 'quote' ) {
                $output .= '<i class="ion-quote"></i><blockquote>';
                $output .= get_the_excerpt();
                $output .= '</blockquote>';
            } elseif ( $format == 'link' ) {
                $output .= '<div class="link-wrap"><i class="ion-link"></i>';
                $output .= get_the_content();
                $output .= '</div>';
            } else {
                $image     = wp_get_attachment_image_url( get_post_thumbnail_id( $post_id ), 'large' );
                $blog_type = cs_get_option( 'blog_type' ) ? cs_get_option( 'blog_type' ) : 'masonry';
                $blog_type = apply_filters( 'chade_blog_style', $blog_type );
                $imgClass  = $blog_type == 'masonry' ? '' : 's-img-switch';
                if ( ! empty( $image ) ) {
                    $output .= '<div class="post-media">';
                    $output .= chade_the_lazy_load_flter( $image, array(
                        'class' => $imgClass,
                        'alt'   => ''
                    ) );
                    $output .= '</div>';
                }

            }
        }
        echo do_shortcode( $output );
    }
}