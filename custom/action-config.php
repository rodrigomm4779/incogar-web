<?php
/**
 * Include reqiured scripts and styles.
 *
 * @package chade
 * @since 1.0.0
 *
 */

add_action( 'widgets_init',       'chade_register_widgets' );
add_action( 'wp_enqueue_scripts', 'chade_enqueue_scripts' );
add_action( 'wp_enqueue_scripts', 'chade_enqueue_main', 9999999999999 );
add_action( 'wp_enqueue_scripts', 'chade_custom_styles', 9999999999999 );
add_action( 'tgmpa_register', 	  'chade_include_required_plugins' );

/* Registr fonts */
function chade_fonts_url() {
    $font_url = '';

    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'chade' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Roboto:300,400,500,700,900' ), "//fonts.googleapis.com/css" );
    }
    return $font_url;
}

/* Register sidebar. */
if( ! function_exists( 'chade_register_widgets' ) ) {
	function chade_register_widgets() {
		// register main sidebar
		register_sidebar(
			array(
				'id'            => 'sidebar',
				'name'          => esc_html__( 'Sidebar', 'chade' ),
				'before_widget' => '<div class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widget--title">',
				'after_title'   => '</h2>',
				'description'   => esc_html__( 'Drag the widgets for sidebars.', 'chade' )
			)
		);
		// register footer sidebar
		if ( function_exists( 'cs_framework_init' ) ) {
			register_sidebar(
				array(
					'id'            => 'footer-sidebar',
					'name'          => esc_html__( 'Footer sidebar', 'chade' ),
					'before_widget' => '<div class="col-lg-3 col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1"><div class="footer_contact_info">',
					'after_widget'  => '</div></div>',
					'before_title'  => '<strong class="footer_main--column_title">',
					'after_title'   => '</strong>',
					'description'   => esc_html__( 'Drag the widgets for sidebars.', 'chade' )
				)
			);
		}
		// register header sidebar
		if ( function_exists( 'cs_framework_init' ) ) {
			register_sidebar(
				array(
					'id'            => 'header-sidebar',
					'name'          => esc_html__( 'Header sidebar', 'chade' ),
					'before_widget' => '<div class="top-header-widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h6 class="top-widget-title">',
					'after_title'   => '</h6>',
					'description'   => esc_html__( 'Drag the widgets for sidebars.', 'chade' )
				)
			);
		}
		// register services sidebar
		if ( function_exists( 'cs_framework_init' ) ) {
			register_sidebar(
				array(
					'id'            => 'services-sidebar',
					'name'          => esc_html__( 'Services sidebar', 'chade' ),
					'before_widget' => '<div class="widget -iconless %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h2 class="widget--title">',
					'after_title'   => '</h2>',
					'description'   => esc_html__( 'Drag the widgets for sidebars.', 'chade' )
				)
			);
		}
	}
}
/**
 * @ return null
 * @ param none
 * @ loads all the js and css script to frontend
 **/

if( ! function_exists( 'chade_enqueue_scripts' ) ) {
	function chade_enqueue_scripts() {

		// general settings
		if ( ( is_admin() ) ) {
			return;
		}

		if ( is_single() ) {
			wp_enqueue_script( 'fitvids-js', CHADE_URI . '/assets/libs/jquery.fitvids.js', array( 'jquery' ), false, true );
		}

		// Preloader style and lib.
		$preloader = chade_get_options( 'preloader' );
		if ( $preloader ) {
			wp_enqueue_style( 'fakeLoader', CHADE_URI . '/assets/styles/fakeLoader.css' );
		}

		$google_map_key = chade_get_options( 'google_map_key', '' );
		if ( ! empty( $google_map_key ) ) {
			wp_enqueue_script( 'google-map', '//maps.googleapis.com/maps/api/js?v=3.exp&key=' . $google_map_key, '', false, true );
		}
		wp_enqueue_script( 'equa', CHADE_URI . '/assets/libs/jquery.equalheights.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'ytbg', CHADE_URI . '/assets/libs/jquery.youtubebackground.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'select2', CHADE_URI . '/assets/libs/select2.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'viewport', CHADE_URI . '/assets/libs/isInViewport.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'progres', CHADE_URI . '/assets/libs/progressbar.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'swiper', CHADE_URI . '/assets/libs/swiper.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'mixitup', CHADE_URI . '/assets/libs/jquery.mixitup.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'tabby', CHADE_URI . '/assets/libs/tabby.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'slick', CHADE_URI . '/assets/libs/slick.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'magnific', CHADE_URI . '/assets/libs/jquery.magnific-popup.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'slideout', CHADE_URI . '/assets/libs/slideout.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'masonry', CHADE_URI . '/assets/libs/masonry.pkgd.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'isotope', CHADE_URI . '/assets/scripts/isotope.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'chade-js', CHADE_URI . '/assets/scripts/main.js', array( 'jquery' ), false, true );

		// add TinyMCE style
		add_editor_style();

		// including jQuery plugins
		wp_localize_script( 'chade-js', 'js_data',
			array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'siteurl' => get_template_directory_uri()
			)
		);

		if ( is_singular() ) {
			wp_enqueue_script( 'comment-reply' );
		}

		// register style
		wp_enqueue_style( 'chade-css', CHADE_URI . '/style.css' );
		wp_enqueue_style( 'chade-fonts', chade_fonts_url(), array() );
		wp_enqueue_style( 'slick', CHADE_URI . '/assets/styles/slick.css' );
		wp_enqueue_style( 'fontawesome-css', CHADE_URI . '/assets/styles/font-awesome.min.css' );
		wp_enqueue_style( 'magnific-css', CHADE_URI . '/assets/styles/magnific-popup.css' );
		wp_enqueue_style( 'social-buttons', CHADE_URI . '/assets/styles/social-buttons.css' );
		wp_enqueue_style( 'swiper', CHADE_URI . '/assets/styles/swiper.css' );

	}
}

// cs framework missing
if ( ! function_exists( 'cs_get_option' ) ) {
    function cs_get_option() {
        return '';
    }
}


// Custom row styles for onepage site type+-.
if ( ! function_exists('chade_dynamic_css' ) ) {

    function chade_dynamic_css() {

        require_once get_template_directory() . '/assets/styles/custom.css.php';

        wp_die();
    }
}


add_action( 'wp_ajax_nopriv_chade_dynamic_css', 'chade_dynamic_css' );
add_action( 'wp_ajax_chade_dynamic_css', 'chade_dynamic_css' );


if ( ! function_exists('chade_enqueue_main' ) ) {
	function chade_enqueue_main() {
		wp_enqueue_style( 'chade-theme-styles', CHADE_URI . '/assets/styles/main.css' );

		if ( is_page() || is_home() ) {
			$post_id = get_queried_object_id();
		} else {
			$post_id = get_the_ID();
		}


		wp_enqueue_style( 'chade_dynamic-css', admin_url( 'admin-ajax.php' ) . '?action=chade_dynamic_css&post=' . $post_id, array( 'chade-theme-styles' ) );
	}
}

/** Custom styles from Theme Options */
if ( ! function_exists( 'chade_custom_styles' ) ) {
    function chade_custom_styles() {
        $styles = '';

        /* Socondary footer background color */
        $sf_color = chade_get_options( 'sf_background_color' );
        if( chade_get_options( 'secondary_footer' ) && $sf_color ) {
            $styles .= '.footer_contacts{background-color:' . esc_attr($sf_color) . ';}';
        }

        // footer title
        $color_title = chade_get_options( 'footer_title_color');
        if( $color_title ) {
            $styles .= '.layout--footer .footer .footer--inner .footer_main .footer_main--column_title{color:' . esc_attr($color_title) . '!important;}';
        }
        // footer text
        $color_text = chade_get_options( 'footer_text_color');
        if ( $color_text ) {
            $styles .= '.layout--footer .footer .footer--inner .footer_main .footer_main--column ul li a{color:' . esc_attr($color_text) . ';}';
            $styles .= '.layout--footer .footer .footer--inner .footer_main .footer_main--column .widget-title{color:' . esc_attr($color_text) . ';}';
            $styles .= '.layout--footer .footer .footer--inner .footer_main .footer_main--column span,
						.layout--footer .footer .footer--inner .footer_main .footer_main--column i{color:' . esc_attr($color_text) . ';}';
            $styles .= '.footer .footer_copyrights .footer_copyrights--item_copyrights{color:' . esc_attr($color_text) . ';}';
            $styles .= '.footer .footer_copyrights .footer_copyrights--container:after{ background-color: ' . esc_attr($color_text) . ';}';
        }

        /* Menu subtext color */
        $menu_subtext_color = chade_get_options( 'menu_subtext_color' );
        if( $menu_subtext_color ) {
            $styles .= '.header_nav .menu > .menu-item > a .menu-item-notify{border-color:' . esc_attr($menu_subtext_color) . ';color:' . esc_attr($menu_subtext_color) . ';}';
        }

        /* Menu icon color */
        $service_icons_color = chade_get_options( 'service_icons_color' );
        if( $service_icons_color ) {
            $styles .= '.mega_nav--item.-icon .mega_nav--item_icon{color:' . esc_attr($service_icons_color) . ';}';
        }

        /* Preloader styles */
        $preloader_bg_color = chade_get_options( 'preloader_bg_color', '#fff' );
        $styles .= '#preloader{background-color:' . esc_attr($preloader_bg_color) . ';}';

        $preloader_items_color = chade_get_options( 'preloader_items_color', '#333' );
        $styles .= '.double-bounce1, .double-bounce2, .container1 > div, .container2 > div, .container3 > div,
			.dot1, .dot2, .spinner4, .cube1, .cube2, .spinner6 > div, .spinner7 > div {background-color:' . esc_attr($preloader_items_color) . ';}';

        /* 404 page Heading background styles */
        $p404_heading_bg = chade_get_options( '404_heading_bg', 'color' );
        if( $p404_heading_bg == 'color' ) {
            $p404_heading_color = chade_get_options( '404_heading_color', '#333' );
            $styles .= '.page-404 > .page_header{ background-color: ' . esc_attr($p404_heading_color) . ';}';
        } else {
            $p404_heading_image = chade_get_options( '404_heading_image', esc_url(CHADE_URI . '/assets/images/pattern-dark.png') );
            $styles .= '.page-404 > .page_header{ background-image: url(' . esc_url($p404_heading_image) . ');}';
        }

        /* Search page Heading background styles */
        $search_heading_bg = chade_get_options( 'search_heading_bg', 'color' );
        if( $search_heading_bg == 'color' ) {
            $search_heading_color = chade_get_options( 'search_heading_color', '#333' );
            $styles .= '.search-page > .page_header{ background-color: ' . esc_attr($search_heading_color) . ';}';
        } else {
            $search_heading_image = chade_get_options( 'search_heading_image', esc_url(CHADE_URI . '/assets/images/pattern-dark.png') );
            $styles .= '.search-page > .page_header{ background-image: url(' . esc_url($search_heading_image) . ');}';
        }

        /* Menu optins */
        $menu_bg_color = chade_get_options('menu_bg_color');
        if( $menu_bg_color ) {
            $styles .= '.header{background-color:' . esc_attr($menu_bg_color) . ';} ';
        }

        $menu_font_color = chade_get_options('menu_font_color');
        if( $menu_font_color ) {
            $styles .= '.header_search i {color:' . esc_attr($menu_font_color) . ';} ';
            $styles .= '.header_nav.-on_dark .header_nav--inner > .menu > .menu-item > a, .header_nav .menu > .menu-item > a{color:' . esc_attr($menu_font_color) . ';} ';
        }

        $menu_active_color = chade_get_options('menu_active_color');
        if( $menu_active_color ) {
            $styles .= '.header_nav .menu > .menu-item > a:before{background:' . esc_attr($menu_active_color) . ';}';
            $styles .= '.header_nav.-on_dark .header_nav--inner > .menu > .menu-item.current-menu-item > a, .header_nav.-wide .header_nav--inner > .menu > .menu-item.current-menu-item > a{color:' . esc_attr($menu_active_color) . ';}';
            $styles .= '.header_nav .menu > .menu-item.current-menu-item > a, .header_nav .menu > .menu-item:hover > a{color:' . esc_attr($menu_active_color) . ';} ';
        }

        $menu_hover_color = chade_get_options('menu_hover_color');
        if( $menu_hover_color ) {
            $styles .= '.header_nav.-on_dark .header_nav--inner > .menu > .menu-item:hover > a ,.header_nav .menu > .menu-item:hover > a {color:' . esc_attr($menu_hover_color) . ';} ';
            $styles .= '.header_search i:hover, .header_nav.-on_dark .header_nav--inner > .menu > .menu-item > a:hover, .header_nav .menu > .menu-item > a:hover{color:' . esc_attr($menu_hover_color) . ';} ';
        }

        /* Text logo font size */
        $text_logo_size = chade_get_options( 'text_logo_size' );
        if( $text_logo_size ) {
            $text_logo_size .= is_numeric( $text_logo_size ) ? 'px' : '';
            $styles .= '.header--logo.logo a{font-size: ' . esc_attr($text_logo_size) . ';}';
        }

        /* Mobile menu custom button */
        //
        $mm_button = chade_get_options( 'mm_button' );
        if( $mm_button ) {
            $mm_button_color = chade_get_options( 'mm_button_color' );
            if( $mm_button_color ) {
                $styles .= '.mobile_sidebar--buttons .button{background-color: ' . esc_attr($mm_button_color) . ';}';
            }

            $mm_button_hover_color = chade_get_options( 'mm_button_hover_color' );
            if( $mm_button_hover_color ) {
                $styles .= '.mobile_sidebar--buttons .button:hover{background-color: ' . esc_attr($mm_button_hover_color) . ';}';
            }
        }

        /* Text logo color */
        $text_logo_color = chade_get_options( 'text_logo_color' );
        if( $text_logo_color ) {
            $styles .= '.header--logo.logo a{color:' . esc_attr($text_logo_color) . ';}';
        }

        /* 	Custom menu button color */
        $tm_button = chade_get_options( 'tm_button' );
        if ( $tm_button ) {
            $tm_button_color = chade_get_options( 'tm_button_color', '#fff' );
            $styles .= '.button.-blue_light.-bordered{color: ' . esc_attr( $tm_button_color ) . ';border-color: ' . esc_attr( $tm_button_color ) . ';}';

            $tm_button_hover_color = chade_get_options( 'tm_button_hover_color', '#333' );
            if( ! empty( $tm_button_hover_color ) ) {
                $styles .= '.button.-blue_light.-bordered:hover{border-color: ' . esc_attr( $tm_button_hover_color ) . ';background-color: ' . esc_attr( $tm_button_hover_color ) . ';}';
            }

        }

        /* Top header color */
        $top_header_color = chade_get_options( 'top_header_color', '#f3f3f3' );
        $styles .= '.layout--header .topbar{background-color: ' . esc_attr( $top_header_color ) . ';}';

        /* Blog Heading background styles */
        $blog_heading = chade_get_options( 'blog_heading' );
        if( $blog_heading ) {
            $blog_heading_bg = chade_get_options( 'blog_heading_bg', 'color' );

            if( $blog_heading_bg == 'color' ) {
                $blog_heading_color = chade_get_options( 'blog_heading_color', '#333' );
                $styles .= '.blog-heading{ background-color: ' . esc_attr($blog_heading_color) . ';}';
            } else {
                $blog_heading_image = chade_get_options( 'blog_heading_image', CHADE_URI . '/assets/images/pattern-dark.png' );
                $blog_img_overlay = chade_get_options( 'blog_img_overlay', 'color' );
                $styles .= '.blog-heading{ background-image: url(' . esc_url($blog_heading_image) . ');position:relative;}';
                $styles .= '.blog-heading:after{content: "";position: absolute;top:0;left:0;width: 100%;height: 100%;background-color: ' . esc_attr($blog_img_overlay) . ';} ';
            }

        }

        /* Post heading */
        if ( is_singular('post') ) {
            $meta_data = get_post_meta( get_the_ID(), 'post_options', true );
            if( isset( $meta_data['post_heading'] ) && $meta_data['post_heading'] ) {
                if( $meta_data['post_heading_bg'] == 'color' ) {
                    $bg_color = '#333';
                    if ( ! empty( $meta_data['post_heading_color'] ) ) {
                        $bg_color = $meta_data['post_heading_color'];
                    }
                    $styles .= '.post-heading{ background-color: ' . esc_attr($bg_color) . ';}';
                } else {
                    $bg_img = CHADE_URI . '/assets/images/pattern-dark.png';
                    if ( ! empty( $meta_data['post_heading_image'] ) ) {
                        $bg_img = $meta_data['post_heading_image'];
                    }
                    $styles .= '.post-heading{ background-image: url(' . esc_url( $bg_img ) . ');}';
                }
            }
        }

        /* Post heading */
        if ( is_singular('services') ) {
            $meta_data = get_post_meta( get_the_ID(), 'services_options', true );
            if( isset( $meta_data['post_heading'] ) && $meta_data['post_heading'] ) {
                if( $meta_data['post_heading_bg'] == 'color' ) {
                    $bg_color = '#383d61';
                    if ( ! empty( $meta_data['post_heading_color'] ) ) {
                        $bg_color = $meta_data['post_heading_color'];
                    }
                    $styles .= '.post-heading{ background-color: ' . esc_attr($bg_color) . ';}';
                } else {
                    $bg_img = CHADE_URI . '/assets/images/pattern-dark.png';
                    if ( ! empty( $meta_data['post_heading_image'] ) ) {
                        $bg_img = $meta_data['post_heading_image'];
                    }
                    $styles .= '.post-heading{ background-image: url(' . esc_url( $bg_img ) . ');}';
                }
            }
        }

        /* Help widget styles */
        $help_background_color = chade_get_options('help_background_color');
        if( $help_background_color && ! empty( $help_background_color ) ) {
            $styles .= '.support_chat--window{ background-color: ' . esc_attr($help_background_color) . ';}';
        }

        $help_title_color = chade_get_options('help_title_color');
        if( $help_title_color && ! empty( $help_title_color ) ) {
            $styles .= '.tip_help--title{color:' . esc_attr($help_title_color) . ';}';
        }

        $help_text_color = chade_get_options('help_text_color');
        if( $help_text_color && ! empty( $help_text_color ) ) {
            $styles .= '.tip_help--text{color:' . esc_attr($help_title_color) . ';}';
        }

        $help_button_color = chade_get_options('help_button_color');
        if( $help_button_color && ! empty( $help_button_color ) ) {
            $styles .= '.tip_help--contact .button.-blue_light.-bordered{color:' . esc_attr($help_button_color) . '; border-color:' . esc_attr($help_button_color) . ';}';
            $styles .= '.tip_help--contact .button.-blue_light.-bordered:hover{background-color:' . esc_attr($help_button_color) . '; color:#fff;}';
        }

        $help_icon_color = chade_get_options('help_icon_color');
        if( $help_icon_color && ! empty( $help_icon_color ) ) {
            $styles .= '.support_chat--opener i{color:' . esc_attr($help_icon_color) . ';}';
        }

        $help_icon_bgcolor = chade_get_options('help_icon_bgcolor');
        if( $help_icon_bgcolor && ! empty( $help_icon_bgcolor ) ) {
            $styles .= '.support_chat--opener {background-color:' . esc_attr($help_icon_bgcolor) . ';}';
        }

        $help_support_name_color = chade_get_options('help_support_name_color');
        if( $help_support_name_color && ! empty( $help_support_name_color ) ) {
            $styles .= '.tip_help--contact_name{color:' . esc_attr($help_support_name_color) . ';}';
        }

        $help_suppotr_position_color = chade_get_options('help_suppotr_position_color');
        if( $help_suppotr_position_color && ! empty( $help_suppotr_position_color ) ) {
            $styles .= '.tip_help--contact_job{color:' . esc_attr($help_suppotr_position_color) . ';}';
        }


        $help_widget_typography = chade_get_options('help_widget_typography');
        if( $help_widget_typography ) {
            $typo = chade_get_options('help_widget_font');
            if( $typo ) {
                if( ! empty( $typo['family'] ) ) {

                    $styles .= '.tip_help--title, .tip_help--text, .tip_help--contact .button.-blue_light.-bordered, .tip_help--contact_name, .tip_help--contact_job {font-family:' . esc_attr($typo['family']) . ';}';

                    if( $typo['font'] == 'google' ) {
                        wp_enqueue_style( 'help_widget_typography', '//fonts.googleapis.com/css?family=' . $typo['family'] );
                    }
                }
            }
        }

        /* Custom typography styles */
        $tags = array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'span', 'a' );
        foreach ( $tags as $tag ) {

            $tag_typography = chade_get_options( $tag . '_custom_typo' );
            if( $tag_typography ) {
                $tag_styles = '';
                $tag_typo = chade_get_options( $tag . '_typo_styles' );
                $font = $tag_typo['typo'];

                if( $tag_typo ) {

                    $fonts  = '';
                    if( isset( $font['font'] ) && $font['font'] == 'google' ) {
                        $variant = ( isset( $font['variant'] ) && $font['variant'] !== 'regular' ) ? ':'. $font['variant'] : '';
                        $fonts = $font['family'] . $variant;
                    }
                    if ( ! empty( $fonts ) ) {
                        wp_enqueue_style( $tag . '_tag_typography', '//fonts.googleapis.com/css?family=' . $fonts );
                    }
                    $font_style = chade_get_str_from_string( $font['variant'] );
                    $font_style = ( $font_style == 'regular' ) ? 'normal' : $font_style;

                    $font_weight = chade_get_number_from_string( $font['variant'] );

                    $tag_styles .= 'font-family:' . $font['family'] . ';';
                    $tag_styles .= ( ! empty( $font_weight ) ) ? 'font-weight:' . esc_attr($font_weight) . ';' : '';
                    $tag_styles .= 'font-style:' . esc_attr($font_style) . ';';
                }

                // Tag font size
                if( ! empty( $tag_typo['font_size'] ) ) {
                    $tag_styles .= 'font-size:' . esc_attr($tag_typo['font_size']) . 'px;';
                }

                // Tag letter spacing
                if( ! empty( $tag_typo['letter_spacing']  ) ) {
                    $tag_styles .= 'letter-spacing:' . esc_attr($tag_typo['letter_spacing']) . 'px;';
                }

                // Tag line height
                if( ! empty( $tag_typo['line_height']  ) ) {
                    $tag_styles .= 'line-height:' . esc_attr($tag_typo['line_height']) . 'px;';
                }

                // Tag font color
                if( ! empty( $tag_typo['color'] ) ) {
                    $tag_styles .= 'color:' . esc_attr($tag_typo['color']) . ';';
                }

                if( ! empty( $tag_styles ) ) {
                    $styles .= 'html body #main ' . $tag . '{' . $tag_styles . '}';
                }
            }
        }

        $custom_css_styles = chade_get_options( 'custom_css_styles' );
        if( $custom_css_styles ) {
            $styles .= $custom_css_styles;
        }

        /* Custom CSS code */
        if ( ! empty( $styles ) ) {
            wp_add_inline_style( 'chade-theme-styles', $styles );
        }

        $custom_js_code = chade_get_options( 'custom_js_code' );
        /* Custom JavaScript code */
        if( ! empty( $custom_js_code ) ) {
            if ( function_exists( 'wp_add_inline_script' ) ) {
                wp_add_inline_script( 'main-js', $custom_js_code );
            }
        }

    }
}

/**
 * Include plugins
 **/
if ( ! function_exists( 'chade_include_required_plugins' ) ) {
    function chade_include_required_plugins() {

        $plugins = array(
            array(
                'name'               => esc_html__( 'Chade Theme Plugin', 'chade' ),
                // The plugin name
                'slug'               => 'chade-plugin',
                // The plugin slug (typically the folder name)
                'source'             => esc_url( 'https://import.foxthemes.me/plugins/chade/chade-plugin.zip' ),
                // The plugin source
                'required'           => true,
                // If false, the plugin is only 'recommended' instead of required
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'version'            => '',
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',
                // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'Visual Composer', 'chade' ),
                // The plugin name
                'slug'               => 'js_composer',
                // The plugin slug (typically the folder name)
                'source'             => esc_url( 'https://import.foxthemes.me/plugins/premium-plugins/js_composer.zip' ),
                // The plugin source
                'required'           => true,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',
                // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'Booked Appointments', 'chade' ),
                // The plugin name
                'slug'               => 'booked',
                // The plugin slug (typically the folder name)
                'source'             => esc_url( 'https://import.foxthemes.me/plugins/premium-plugins/booked.zip' ),
                // The plugin source
                'required'           => false,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',
                // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'The Grid', 'chade' ),
                // The plugin name
                'slug'               => 'the_grid',
                // The plugin slug (typically the folder name)
                'source'             => esc_url( 'https://import.foxthemes.me/plugins/premium-plugins/the_grid.zip' ),
                // The plugin source
                'required'           => true,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',
                // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'WPML', 'chade' ),
                // The plugin name
                'slug'               => 'wpml',
                // The plugin slug (typically the folder name)
                'source'             => esc_url( 'https://import.foxthemes.me/plugins/premium-plugins/wpml.zip' ),
                // The plugin source
                'required'           => true,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',
                // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'Slider Revolution', 'chade' ),
                // The plugin name
                'slug'               => 'revslider',
                // The plugin slug (typically the folder name)
                'source'             => esc_url( 'https://import.foxthemes.me/plugins/premium-plugins/revslider.zip' ),
                // The plugin source
                'required'           => true,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',
                // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'Modern Events Calendar', 'chade' ),
                // The plugin name
                'slug'               => 'modern-events-calendar',
                // The plugin slug (typically the folder name)
                'source'             => esc_url(  'https://import.foxthemes.me/plugins/premium-plugins/modern-events-calendar.zip' ),
                // The plugin source
                'required'           => true,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',
                // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'Contact Form 7', 'chade' ),
                // The plugin name
                'slug'               => 'contact-form-7',
                // The plugin slug (typically the folder name)
                'required'           => false,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',
                // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'UpQode Google Maps', 'chade' ),
                // The plugin name
                'slug'               => 'upqode-google-maps',
                // The plugin slug (typically the folder name)
                'required'           => false,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',
                // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'MailChimp for WordPress', 'chade' ),
                // The plugin name
                'slug'               => 'mailchimp-for-wp',
                // The plugin slug (typically the folder name)
                'required'           => false,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',
                // If set, overrides default API URL and points to an external URL
            )
        );

        // Change this to your theme text domain, used for internationalising strings

        /**
         * Array of configuration settings. Amend each line as needed.
         * If you want the default strings to be available under your own theme domain,
         * leave the strings uncommented.
         * Some of the strings are added into a sprintf, so see the comments at the
         * end of each line for what each argument will be.
         */
        $config = array(
            'domain'       => 'chade',                    // Text domain - likely want to be the same as your theme.
            'default_path' => '',                            // Default absolute path to pre-packaged plugins
            'menu'         => 'tgmpa-install-plugins',    // Menu slug
            'has_notices'  => true,                        // Show admin notices or not
            'is_automatic' => true,                        // Automatically activate plugins after installation or not
            'message'      => '',                            // Message to output right before the plugins table
            'strings'      => array(
                'page_title'                      => esc_html__( 'Install Required Plugins', 'chade' ),
                'menu_title'                      => esc_html__( 'Install Plugins', 'chade' ),
                'installing'                      => esc_html__( 'Installing Plugin: %s', 'chade' ),
                // %1$s = plugin name
                'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'chade' ),
                'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'chade' ),
                // %1$s = plugin name(s)
                'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'chade' ),
                // %1$s = plugin name(s)
                'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'chade' ),
                // %1$s = plugin name(s)
                'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'chade' ),
                // %1$s = plugin name(s)
                'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'chade' ),
                // %1$s = plugin name(s)
                'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'chade' ),
                // %1$s = plugin name(s)
                'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'chade' ),
                // %1$s = plugin name(s)
                'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'chade' ),
                // %1$s = plugin name(s)
                'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'chade' ),
                'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'chade' ),
                'return'                          => esc_html__( 'Return to Required Plugins Installer', 'chade' ),
                'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'chade' ),
                'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'chade' ),
                // %1$s = dashboard link
                'nag_type'                        => 'updated'
                // Determines admin notice type - can only be 'updated' or 'error'
            )
        );

        tgmpa( $plugins, $config );
    }
}

/*
 * Add backend styles for Gutenberg.
 */
add_action( 'enqueue_block_editor_assets', 'chade_add_gutenberg_assets' );

if ( ! function_exists( 'chade_add_gutenberg_assets' ) ) {
	function chade_add_gutenberg_assets() {

		// Load the theme styles within Gutenberg.

		wp_enqueue_style( 'chade-fonts', chade_fonts_url(), array() );
		wp_enqueue_style( 'fontawesome-css', CHADE_URI . '/assets/styles/font-awesome.min.css' );
		wp_enqueue_style( 'chade-gutenberg', CHADE_URI . '/assets/styles/gutenberg-editor-style.css');

	}
}
