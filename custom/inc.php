<?php
/**
 * Including needed files.
 *
 * @package wpc
 * @since 1.0.0
 *
 */

// Helper functions.
require_once CHADE_T_PATH . '/custom/helper-functions.php';

// Include all styles and scripts.
require_once CHADE_T_PATH . '/custom/action-config.php';

// Add additional options for admin menu.
require_once CHADE_T_PATH . '/custom/admin-menu-walker.php';

// Change main menu Walker for additional options.
require_once CHADE_T_PATH . '/custom/front-end-menu-walker.php';

// Plugin activation class.
require_once CHADE_T_PATH . '/custom/class-tgm-plugin-activation.php';