<?php
/**
 * Index Page
 *
 * @package chade
 * @since 1.0.0
 *
 */
// Sidebar option.
$page_sidebar = chade_get_options( 'page_sidebar', false ) && class_exists('Vc_Manager') ? chade_get_options( 'page_sidebar', false ) : false;

// Check if sidebar enable and put correct class for main content area.
$content_width_class = $page_sidebar ? 'col-md-9' : 'col-md-12';

get_header();

while ( have_posts() ) : the_post();

	$content = get_the_content(); ?>
	<div class="layout--container">
		<?php // Check if content has visual composer or woocommerce shortcodes.
		if ( ! strpos( $content, 'vc_' ) && ! strpos( $content, 'woocommerce_' ) ) { ?>

			<div class="page_header" style="background-image: url(<?php echo CHADE_URI . '/assets/images/pattern-dark.png'; ?>);">
                <div class="container">
                    <h1 class="page_header--title"><?php esc_html(the_title()); ?></h1>
                </div>
            </div>

			<?php chade_breadcrumbs(); ?>

			<section style="padding-top: 40px" class="section page-content">
				<div class="container">
					<div class="row">
						<div class="<?php echo esc_attr( $content_width_class ); ?> col-xs-12">
							<!-- Post body -->
							<div class="article">

								<!-- Post contant area -->
								<div class="article--content clearfix">
									<?php the_content(); ?>
                                </div>
								<?php wp_link_pages('before=<div class="post-nav"> <span>' . esc_html__( 'Page:', 'chade' ) . ' </span> &after=</div>'); ?>
                                <!-- End post contant area -->
							</div>
							<?php if ( comments_open() ) { ?>
								<!-- Comments -->
								<?php comments_template(); ?>
								<!-- End comments -->
							<?php } ?>
						</div>

						<?php if ( $page_sidebar ): ?>
							<!-- Blog sidebar -->
							<div class="col-md-3 col-xs-12">
								<div class="blog_sidebar">
									<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar('sidebar') ); ?>
								</div>
							</div>
							<!-- End blog sidebar -->
						<?php endif ?>

					</div>
				</div>
			</section>
		<?php } else { ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
<?php endwhile;

get_footer(); ?>